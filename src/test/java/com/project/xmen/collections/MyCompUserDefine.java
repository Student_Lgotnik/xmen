package com.project.xmen.collections;



import org.junit.Test;

import java.util.TreeSet;

public class MyCompUserDefine {

    @Test
    public void test() {

        TreeSet<Integer> treeSet = new TreeSet<>();
        treeSet.add(6);
        treeSet.add(2);
        treeSet.add(4);

//        treeSet.forEach(x -> System.out.println(x));
        treeSet.forEach(System.out::println);

        TreeSet<Empl> nameComp = new TreeSet<>(new MyNameComp());
        nameComp.add(new Empl("Ram", 3800));
        nameComp.add(new Empl("John", 6000));
        nameComp.add(new Empl("Chris", 2000));
        nameComp.add(new Empl("Tom", 2400));

        for (Empl e : nameComp){
            System.out.println(e);
        }

        nameComp.forEach(e -> System.out.println(e));

        System.out.println("================");
        TreeSet<Empl> salaryComp = new TreeSet<>(new MySalaryComp());
        nameComp.add(new Empl("Ram", 3800));
        nameComp.add(new Empl("John", 6000));
        nameComp.add(new Empl("Chris", 2000));
        nameComp.add(new Empl("Tom", 2400));

        for (Empl e: salaryComp){
            System.out.println(e);
        }
    }


}
