package com.project.xmen.innner;

import com.project.xmen.inner.Outer;
import com.sun.corba.se.impl.orbutil.ObjectUtility;
import org.junit.Test;

public class OutherTest {

    public static int x;
    public static int x1;
    public int y;

    //Static блок инициализирует только статические данные
    //Инициализирует переменные первее всех
    static {
        x = 3;
        //y = 10;
    }
    //Простой блок инициализирует все данные данные
    {
        x1 = 6;
        y = 5;
    }

    public void print() {
        System.out.println(x + x1 + y);
    }

    public static void  print2() {
        System.out.println(x + x1);
    }

    @Test public void test() {
        OutherTest outherTest = new OutherTest();
        outherTest.print();
        OutherTest.print2();
    }

    @Test
    public void outherTest() {

        Outer.Nested nested = new Outer.Nested();
        nested.printNestedText();

        Outer outer = new Outer();
        Outer.Inner inner = outer.new Inner();
        inner.printText();

        Outer outer1 = new Outer() {

            @Override
            public void doIt() {
                System.out.println("Anonymous class doIt()");
            }
        };

        outer.doIt();
        outer1.doIt();
    }
}
