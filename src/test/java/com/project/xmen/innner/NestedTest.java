package com.project.xmen.innner;

import de.bechte.junit.runners.context.HierarchicalContextRunner;
import org.junit.*;
import org.junit.runner.RunWith;

@RunWith(HierarchicalContextRunner.class)
public class NestedTest {

    @BeforeClass
    public static void befreAllTestMethods() {
        System.out.println("Invoked once before all test methods");
    }

    @Before
    public void befreEatchTestMethod() {
        System.out.println("Invoked before each test method");
    }

    @After
    public void afterEatchTestMethod() {
        System.out.println("Invoked after each test method");
    }

    @AfterClass
    public static void afterAllTestMethods() {
        System.out.println("Invoked once after all test method");
    }

    @Test
    public void rootClassTest() {
        System.out.println("Root class test!");
    }

    public class ContextA {

        @Before
        public void beforeEachTestMethodOfContextA() {
            System.out.println("Invoked once before all test methods of contextA");
        }

        @After
        public void afterEachTestMethodOfContextA() {
            System.out.println("Invoked after each test method of contextA");
        }

        @Test
        public void contextATest() {
            System.out.println("Context A test");
        }

        public class ContextC {

            @Before
            public void beforeEachTestMethodOfContextC() {
                System.out.println("Invoked once before all test methods of contextC");
            }

            @After
            public void afterEachTestMethodOfContextC() {
                System.out.println("Invoked after each test method of contextC");
            }

            @Test
            public void contextCTest() {
                System.out.println("Context C test");
            }

        }

    }

}
