package com.project.xmen.functional;


import org.junit.Test;

import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.List;
import java.util.function.*;

public class SimpleFunInterfaceTest {

    @Test
    public void test() {

        checkWork(new SimpleFunctioanlInterface() {
            @Override
            public void doWork() {
                System.out.println("Do work in SimpleFun impl...");
            }
        });

        checkWork(() -> System.out.println("Do work in lambda exp ipl..."));


    }

    private static void checkWork(SimpleFunctioanlInterface simpleFunctioanlInterface){

        int y = SimpleFunctioanlInterface.x;
        simpleFunctioanlInterface.doWork();

    }

    @Test
    public void predicateTest() {

        Predicate<Integer> isPositive = x -> x > 0;

        System.out.println(isPositive.test(5));
        System.out.println(isPositive.test(-7));

        List<Integer> list = Arrays.asList(1, 2, 3, 4, 5, 6, 7,8 ,9);

        System.out.println("Print all numbers");

        //pass n as parameter
        eval(list, n -> true);

        System.out.println("Print even numbers: ");
        eval(list, n -> n%2 == 0);

        System.out.println("Print numbers grater than 3: ");
        eval(list, n -> n > 3);

    }

    public static void eval(List<Integer> list, Predicate<Integer> predicate){

        for (Integer n : list){
            if (predicate.test(n)){
                System.out.println(n + " ");
            }
        }


    }

    @Test
    public void binaryOperator() {
        BinaryOperator<Integer> multiply = (x, y) -> x * y;

        System.out.println(multiply.apply(3, 5));
        System.out.println(multiply.apply(10, -2));
    }

    @Test
    public void unaryOperator() {
        UnaryOperator<Integer> square = x -> x*x;
        System.out.println(square.apply(5));
    }

    @Test
    public void function() {
        Function<Integer, String> convert = x -> String.valueOf(x) + " euro";
        System.out.println(convert.apply(5));
    }

    @Test
    public void consumer() {
        Consumer<Integer> printer = x -> {
            Integer integer = new Integer(x + 5);
            System.out.println(integer);
            System.out.printf("%d euro \n", x);
        };

        printer.accept(600);
    }

}
