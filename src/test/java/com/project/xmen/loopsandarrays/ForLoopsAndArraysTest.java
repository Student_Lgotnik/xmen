package com.project.xmen.loopsandarrays;

import org.junit.Test;
import static com.project.xmen.loopsandarrays.ForLoopsAndArrays.*;
import static com.project.xmen.firstapp.FirstApp.*;

public class ForLoopsAndArraysTest {
    @Test
    public void checkingEqualityOfTwoArrays(){
        int[] array1 = null;
        int[] array2 = {2, 5, 7, 8, 11};
        int[] array3 = {2, 5, 7, 9, 11};
        int[] array4 = {2, 5, 7, 8, 11};

        System.out.println(equalsOfTwoArrays(array2, array3));

    }
    @Test
    public void uniqueElements(){
        uniqueArray(new int[] {0, 3, -2, 4, 3, 2});
        println();
        println();

        uniqueArray(new int[] {10, 22, 10, 20, 11, 22});
    }
}
