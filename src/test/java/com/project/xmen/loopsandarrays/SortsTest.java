package com.project.xmen.loopsandarrays;

import org.junit.Test;
import static com.project.xmen.loopsandarrays.Sorts.*;

public class SortsTest {
    @Test
    public void sorts(){
        Integer[] arr = {5, 9, 1, 10, 34, 15};
        Integer[] res1 = bubbleSort(arr);
        Integer[] res2 = shakerSort(arr);
        for (int i = 0; i < arr.length; i++) {
            System.out.print(res1[i] + " ");
        }
        System.out.println();
        for (int i = 0; i < arr.length; i++) {
            System.out.print(res2[i] + " ");
        }
    }
}
