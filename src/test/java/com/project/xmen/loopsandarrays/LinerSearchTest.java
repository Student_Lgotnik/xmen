package com.project.xmen.loopsandarrays;

import org.junit.Test;
import static com.project.xmen.loopsandarrays.LinerSearch.*;

public class LinerSearchTest {
    @Test
    public void linerTest(){
        int[] arr1 = {11, 23, 47, 133, 99, 23};
        String[] arr2 = {"a", "xxx", "yes", "loop", "no", "xmen"};

        System.out.println("Searched at possition: " + sequentialSearch(arr1, 99));
        System.out.println("Searched at possition: " + sequentialSearch(arr1, 47));
        System.out.println("Searched at possition: " + sequentialSearch(arr1, 11));
        System.out.println();
        System.out.println("Searched at possition: " + sequentialSearch(arr2, "no"));
        System.out.println("Searched at possition: " + sequentialSearch(arr2, "yes"));
        System.out.println("Searched at possition: " + sequentialSearch(arr2, "a"));
    }
}
