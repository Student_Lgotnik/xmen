package com.project.xmen.conditional;

import org.junit.Test;
import static com.project.xmen.conditional.ConditionalStatments.*;

public class ConditionalStatmentsTest {
    @Test
    public void posOrNegTest(){
        Integer x = -6;
        Integer y = 10;
        Integer z = 0;
        positiveOrNegative(x);
        positiveOrNegative(y);
        positiveOrNegative(z);
    }
}
