package com.project.xmen.inheritance;

import org.junit.Test;

public class CircleTest {
    @Test
    public void circle() {

        Cylinder cl = new Cylinder();
        System.out.println("Cylinder: "
                + " radius=" + cl.getRadius()
                + " heigt=" + cl.getHeight()
                + " base area=" + cl.getArea()
                + " volume=" + cl.getValume());

        Cylinder c2 = new Cylinder(10.0);
        System.out.println("Cylinder: "
                + " radius=" + c2.getRadius()
                + " heigt=" + c2.getHeight()
                + " base area=" + c2.getArea()
                + " volume=" + c2.getValume());


        Cylinder c3 = new Cylinder(2.0, 10.0);
        System.out.println("Cylinder: "
                + " radius=" + c3.getRadius()
                + " heigt=" + c3.getHeight()
                + " base area=" + c3.getArea()
                + " volume=" + c3.getValume());

        System.out.println(cl.toString());
    }
}
