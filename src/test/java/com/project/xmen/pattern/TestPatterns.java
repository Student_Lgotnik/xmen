package com.project.xmen.pattern;

import com.project.xmen.pattern.abstractfactory.AbstractFactory;
import com.project.xmen.pattern.abstractfactory.SpeciesFactory;
import com.project.xmen.pattern.builder.*;
import com.project.xmen.pattern.factory.Animal;
import com.project.xmen.pattern.factory.AnimalFactory;
import com.project.xmen.pattern.prototype.Person;
import com.project.xmen.pattern.singleton.SingletonExample;
import org.junit.Assert;
import org.junit.Test;

import java.util.Arrays;

public class TestPatterns {
    @Test
    public void builder(){
        Student s =new Student.Builder().age(27).language(Arrays.asList("chinese","english")).name("aleksey").build();
        System.out.println(s);

        Student student = new Student.Builder().age(45).build();
    }

    @Test
    public void singleton(){

        SingletonExample singletonExample1 = SingletonExample.getInstance();
        singletonExample1.sayHello();

//        SingletonExample singletonExample = SingletonExample.getInstance();
//        singletonExample.sayHello();
    }

    @Test
    public void factory(){

        AnimalFactory animalFactory = new AnimalFactory();

        Animal a1 = animalFactory.getAnimal("feline");
        System.out.println("a1 sound: " + a1.makeSound());

        Animal a2 = animalFactory.getAnimal("canine");
        System.out.println("a2 sound: " + a2.makeSound());
    }

    @Test
    public void abstractFactory(){
        AbstractFactory abstractFactory = new AbstractFactory();

        SpeciesFactory speciesFactory1 = abstractFactory.getSpeciesFactory("reptile");

        Animal a1 = speciesFactory1.getAnimal("tyrannosaurus");
        System.out.println("a1 sound: " + a1.makeSound());
        assert a1.makeSound().equals("Roar");

        Animal a2 = speciesFactory1.getAnimal("snake");
        System.out.println("a2 sound: " + a2.makeSound());
        assert a2.makeSound().equals("Hiss");

        SpeciesFactory speciesFactory2 = abstractFactory.getSpeciesFactory("mammal");

        Animal a4 = speciesFactory2.getAnimal("dog");
        System.out.println("a4 sound: " + a4.makeSound());
        Assert.assertEquals("Woof",a4.makeSound());


        Animal a3 = speciesFactory2.getAnimal("cat");
        System.out.println("a3 sound: " + a3.makeSound());
        Assert.assertEquals("Meow",a3.makeSound());



        //Всю выше поебень можно заменить вот так
        Animal animal = new AbstractFactory()
                .getSpeciesFactory("mammal")
                .getAnimal("dog");
        System.out.println(animal.makeSound());

    }


    @Test
    public void prototype(){
        Person person = new Person("Fred");
        System.out.println("person 1: " + person);
        Person person1 = (Person) person.doClone();
        System.out.println("person 2:" + person1);

    }

    @Test
    public void builder2() {
        MealBuilder mealBuilder = new ItalianMealBuilder();
        MealDirector mealDirector = new MealDirector(mealBuilder);
        mealDirector.constructMeal();
        Meal meal = mealDirector.getMeal();
        System.out.println("meal is:" + meal);

        mealBuilder = new JapaneseMealBuilder();
        mealDirector = new MealDirector(mealBuilder);
        mealDirector.constructMeal();
        meal = mealDirector.getMeal();
        System.out.println("meal is:" + meal);


        //Как можно короче заменить верхнюю поебень
        //Decorator
        Meal x = new MealDirector(new JapaneseMealBuilder()).constructMeal().getMeal();
        System.out.println(x);
    }
}
