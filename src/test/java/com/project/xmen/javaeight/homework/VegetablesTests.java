package com.project.xmen.javaeight.homework;

import org.junit.Test;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collector;
import java.util.stream.Collectors;

public class VegetablesTests {

    @Test
    public void test() {

        List<Vegetable> vegetables = Arrays.asList(
                new Vegetable(1, "Tomato", "Red", 56),
                new Vegetable(2, "Cucumber", "Green", 61),
                new Vegetable(3, "Radish", "Pink", 46),
                new Vegetable(4, "Carrot", "Orange", 33),
                new Vegetable(5, "Potato", "Brown", 24)
        );

        List<Vegetable> modified = vegetables.stream().filter(x -> {
            if (x.getId() == 3)
                x.setId(1);
            return true;
        }).collect(Collectors.toList());
        System.out.println(modified);

        Map<Integer, String> idGrouped = vegetables.stream().collect(Collectors
                .toMap(Vegetable::getId, Vegetable::toString, (v1 , v2) -> v1 + "; " + v2));
        System.out.println(idGrouped);

        List<Integer> ids = vegetables.stream().map(Vegetable::getId).distinct().collect(Collectors.toList());
        System.out.println(ids);

        List<Vegetable> unique = vegetables.stream().filter(distinctByKey(Vegetable::getId)).collect(Collectors.toList());
        System.out.println(unique);



    }

    public static <T> Predicate<T> distinctByKey(Function<? super T, Object> keyExtractor)
    {
        Map<Object, Boolean> map = new ConcurrentHashMap<>();
        return t -> map.putIfAbsent(keyExtractor.apply(t), Boolean.TRUE) == null;
    }

}
