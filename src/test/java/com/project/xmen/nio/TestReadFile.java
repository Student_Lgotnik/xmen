package com.project.xmen.nio;



import org.junit.Test;

import java.awt.*;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class TestReadFile {

    final static String POM = "/home/student/HIllel/classwork/xmen/pom.xml";

    @Test
    public void testRead() {

        try(Stream<String> stream = Files.lines(Paths.get(POM))){
            stream.forEach(System.out::println);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    @Test
    public void testSecondReader() {


        List<String> list = new ArrayList<>();

        try (Stream<String> stream = Files.lines(Paths.get(POM))){

            //1. filter line
            //2. convert all to upper case
            //3. convert it into a list
            list = stream.filter(line -> line.startsWith("<"))
                    .map(String::toUpperCase)
                    .collect(Collectors.toList());
        } catch (IOException e) {
            e.printStackTrace();
        }
        list.forEach(System.out::println);
    }

    @Test
    public void testReference() {

        ArrayList<Integer> arrayList = new ArrayList<>();
        arrayList.add(2);
        arrayList.add(1);
        arrayList.add(1);
        arrayList.add(1);
        arrayList.add(1);
        arrayList.add(1);
        arrayList.add(1);
        arrayList.add(1);

        arrayList.forEach(TestReadFile::square);
        arrayList.forEach(new TestReadFile()::square2);

    }

    public static void square(int num){
        System.out.println(Math.pow(num, 2));
    }

    public void square2(int num) {
        System.out.println(Math.pow(num, 2));
    }

    @Test
    public void paths() throws IOException {

        Path path = Paths.get(NioTest.PROJECT_PATH, "paths.txt");
        String question = "Be or not to be?";
        Files.write(path, question.getBytes());

        Path path2 =Paths.get(NioTest.PROJECT_PATH, "paths.txt");
        System.out.println(path2.getFileName() + " " +
                path2.getName(0) + " " +
                path2.getNameCount() + " " +
                path2.subpath(0,2) + " " +
                path2.getParent() + " " +
                path2.getRoot());

        TestReadFile.convertPths();
    }

    private static void convertPths(){
        Path relarive = Paths.get("src/main/resources/MyZIP.zip");
        System.out.println("Relative path: " + relarive);
        Path absolute = relarive.toAbsolutePath();
        System.out.println("Absolute pathh: " + absolute);
    }

    @Test
    public void copy() {
        Path oldFile = Paths.get("/home/student/HIllel/classwork/xmen/","paths.txt");
        Path newFile = Paths.get("src/main/resources", "newFile.txt");
        try(OutputStream os = new FileOutputStream(newFile.toFile())) {
            Files.copy(oldFile, os);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
