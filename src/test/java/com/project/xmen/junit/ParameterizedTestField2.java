package com.project.xmen.junit;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import static org.junit.Assert.assertEquals;


import java.util.Arrays;
import java.util.Collection;

@RunWith(Parameterized.class)
public class ParameterizedTestField2 {

    private int m1;
    private int m2;

    public ParameterizedTestField2(int m1, int m2) {
        this.m1 = m1;
        this.m2 = m2;
    }

    @Parameterized.Parameters
    public static Collection<Object[]> data() {
        Object[][] data = new Object[][]{ {1, 2}, {5, 3}, {121, 4}};
        return Arrays.asList(data);
    }

    @Test
    public void testMultiplyException() {
        assertEquals("Result", m1 * m2, DataMethods.multiply(m1, m2));
    }

}
