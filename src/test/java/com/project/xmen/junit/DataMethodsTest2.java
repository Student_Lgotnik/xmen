package com.project.xmen.junit;

import org.junit.*;

import javax.xml.crypto.Data;

import static org.junit.Assert.assertEquals;

public class DataMethodsTest2 {

    @BeforeClass
    public static void setUpBeforeClass() throws Exception {
        System.out.println("before class");
    }

    @Before
    public void setUp() throws Exception {
        System.out.println("before");
    }

    @Test
    public void testFindMax() throws Exception {
        System.out.println("test case find max");
        assertEquals(4, DataMethods.findMax(new int[]{1,4,2,3,}));
        assertEquals(-1, DataMethods.findMax(new int[]{-12,-1,-3,-4,-2}));

    }

    @Test
    public void testReversMethod() {
        System.out.println("test case reverse method");
        assertEquals("ym eman si nahk ", DataMethods.reverseWord("my name is khan"));
    }

    @After
    public void tearDown () throws Exception {
        System.out.println("after");
    }

    @AfterClass
    public static void tesrDownAfterclass() throws Exception{
        System.out.println("after class");
    }

}
