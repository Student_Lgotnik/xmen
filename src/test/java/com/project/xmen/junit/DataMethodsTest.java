package com.project.xmen.junit;

import static org.junit.Assert.assertEquals;
import org.junit.Test;
import com.project.xmen.junit.DataMethods;

public class DataMethodsTest {

    @Test
    public void testFindMax(){

        assertEquals(4, DataMethods.findMax(new int[]{1,4,2,3,}));
        assertEquals(-1, DataMethods.findMax(new int[]{-12,-1,-3,-4,-2}));

        int test = DataMethods.findMax(new int[]{1,4,2,3,5});
        assert test == 5;

    }

    @Test
    public void multiplicationOfZero() {

        assertEquals("10 x 0 must be 0", 0, DataMethods.multiply(10, 0));
        assertEquals("0 x 10 must be 0", 0, DataMethods.multiply(0, 10));
        assertEquals("0 x 0 must be 0", 0, DataMethods.multiply(0, 0));

    }

}
