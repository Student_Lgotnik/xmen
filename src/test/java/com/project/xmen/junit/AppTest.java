package com.project.xmen.junit;


import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.*;

import java.util.function.Supplier;

public class AppTest {

    @BeforeAll
    static void setup() {
        System.out.println("@BeforeAll executed");
    }

    @BeforeEach
    void setupThis() {
        System.out.println("@BeforeEath executed");
    }

    @Tag("DEV")
    @Test
    void testCalcOne() {
        System.out.println("=======TEST ONE EXECUTED========");
        Assertions.assertEquals(4, DataMethods.sum(2, 2));
    }

    @Tag("PROD")
    @Disabled
    @Test
    void testCalcTwo() {
        System.out.println("========TEST TWO EXECUTED========");
        Assertions.assertEquals(6, DataMethods.sum(2, 4));
    }

    @AfterAll
    static void tear() {
        System.out.println("@AfterAll executed");
    }

    @Tag("DEV")
    @Test
    void testCase() {
        Assertions.assertNotEquals(3, DataMethods.sum(2, 2));

        Assertions.assertNotEquals(5, DataMethods.sum(2, 2), "DataMethods.sum(2, 2) test failed");
        Supplier<String> massgeSupplier = () -> "DataMethodsTest.sum(2, 2) test failed";
        Assertions.assertNotEquals(5, DataMethods.sum(2, 2), massgeSupplier);

    }

}
