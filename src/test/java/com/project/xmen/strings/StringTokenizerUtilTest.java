package com.project.xmen.strings;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static com.project.xmen.strings.StringTokenizerUtil.*;
public class StringTokenizerUtilTest {
    @Test
    public void tokenize(){
        String str = "Hello World OK";
        String str2 = "Hello, World, OK";

        StringTokenizerUtil.splitBySpace(str);
        StringTokenizerUtil.splitByComma(str2);
    }
    @Test
    public void readCSV(){
        String path = "/home/student/HIllel/classwork/xmen/src/main/resources/file.csv";
        ReadFile.readCsv(path);
    }

    @Test
    public void writeTxt(){
        String path = "/home/student/HIllel/classwork/xmen/src/main/resources/Emloyee.txt";
        List<Employee> employees = new ArrayList<>();
        for (int i = 1; i <= 10; i++) {
            Employee employee = new Employee(
                    "name" + i,
                    "surname" + i,
                    "position" + i,
                    i + "@gmail.com",
                    20 + i,
                    i * 10000,
                    (i % 10) < 5 ? false : true) ;
            employees.add(employee);
        }
        WriteFile.writeTxt(path, employees);
    }
}
