package com.project.xmen.generics;

import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class TestGen {

    @Test
    public void test() {
        GenericSmpl<String> genericSmpl = new GenericSmpl<>();
        genericSmpl.test("Test");

        GenericSmpl<Integer> genericSmpl1 = new GenericSmpl<>();
        genericSmpl1.test(5);

    }

    @Test
    public void test2() {
        List<Integer> list = new ArrayList<>();
        list.add(5);

        List<Dog> dogList = new ArrayList<>();
        dogList.add(new Dog().name("Sharik"));

        List<Animal> animals = new ArrayList<>();

        Animal animal = new Animal() {

            String name;

            @Override
            Object name(Object o) {
                this.name = (String)o;
                return this;
            }

            @Override
            Object getData() {
                return this.name;
            }

            @Override
            public String toString() {
                return "$classname{" +
                        "name='" + name + '\'' +
                        '}';
            }
        };

        animal.name("Zhiraf");
        animals.add(animal);

        collection(list);
        collection2(dogList);
        collection3(animals);


    }

    public void collection(List<?> list){
        list.forEach(System.out::println);
    }

    public void collection2(List<? extends Animal> list){
        list.forEach(x -> System.out.println(x.getClass()));
    }

    public void collection3(List<? super Dog> list){
        list.forEach(x -> System.out.println(x.toString()));
    }

}
