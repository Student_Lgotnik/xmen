package com.project.xmen.interfaces;

import org.junit.Test;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class SortFruitObject {

    @Test
    public void sortFruitObjects(){

        List<Fruit> fruits = new ArrayList<>();

        Fruit pineapple = new Fruit("Pineapple", "Pineapple description", 70);
        Fruit apple = new Fruit("Apple", "Apple description", 100);
        Fruit orange = new Fruit("Orange", "Orange description", 80);
        Fruit banana = new Fruit("Banana", "Banana description", 90);
        Fruit apple2 = new Fruit("Apple", "Apple description", 110);

        fruits.add(pineapple);
        fruits.add(apple);
        fruits.add(orange);
        fruits.add(banana);
        fruits.add(apple2);

//        Collections.sort(fruits);
//        for (Fruit fruit : fruits){
//            System.out.println(fruit.getFruitName() + " " + fruit.getQuantity());
//        }

        Collections.sort(fruits, Fruit.fruitComparator);

        for (Fruit temp : fruits){
            System.out.println("fruits " + " : " + temp.getFruitName() +
                                ", Quantity : " + temp.getQuantity());
        }

    }

}
