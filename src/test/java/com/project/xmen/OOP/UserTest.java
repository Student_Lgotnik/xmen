package com.project.xmen.OOP;

import org.junit.Test;
import static org.junit.Assert.assertEquals;

public class UserTest {

    @Test
    public void setPasswordNormal(){
        User u = new User();
        u.setPassword("qwerty");
        assertEquals("qwerty", u.getPassword());
    }

    @Test
    public void serPasswordShort(){
        User u = new User();
        u.setPassword("qwert");
        assertEquals(null, u.getPassword());
    }

    @Test
    public void setPasswordNull1(){
        User u = new User();
        u.setPassword(null);
        assertEquals(null, u.getPassword());
    }

    @Test(expected = NullPointerException.class)
    public void setPasswordNull2(){
        User u = new User();
        u.setPassword(null);
        assertEquals(null, u.getPassword());
    }

    @Test
    public void setLoginIdValid() {
        User u = new User();
        u.setLoginId("a123");
        assertEquals("a123", u.getLoginId());
    }

    @Test
    public void setLoginIdInvalid() {
        User u = new User();
        u.setLoginId("@#$@$@");
        assertEquals(null, u.getLoginId());
    }

}
