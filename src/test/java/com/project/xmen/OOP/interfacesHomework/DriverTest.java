package com.project.xmen.OOP.interfacesHomework;

import com.project.xmen.OOP.InterfacesHomework.Doctor;
import com.project.xmen.OOP.InterfacesHomework.Driver;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static junit.framework.TestCase.assertEquals;

public class DriverTest {

    @Test
    public void testDriverSorting(){
        Driver d1 = new Driver("Leo", "Yangelay str.", "+380991237380", 3,"1r35", 5600,"Leo license");
        Driver d2 = new Driver("Tom", "Bilinskogo str.", "+380992235980", 1, "1r35", 5700,"Tom license");
        Driver d3 = new Driver("Bob", "Karalenko str.", "+380550128738", 4, "1r35", 5300,"Bob license");
        Driver d4 = new Driver("Tolay", "Shevchenko str.", "+380991237380", 5, "1r35", 6700,"Tolay license");
        Driver d5 = new Driver("Max", "Kulbesheva str.", "+380661212389", 2, "1r35", 12300,"Max license");

        List<Driver> listOfDrivers = new ArrayList<>();

        listOfDrivers.add(d1);
        listOfDrivers.add(d2);
        listOfDrivers.add(d3);
        listOfDrivers.add(d4);
        listOfDrivers.add(d5);
        System.out.println("Unsorted List : " + listOfDrivers);

        Collections.sort(listOfDrivers);
        assertEquals(d2, listOfDrivers.get(0));
        assertEquals(d4, listOfDrivers.get(4));

        System.out.println("Sorted list: " + listOfDrivers);

        Collections.sort(listOfDrivers, Driver.nameComparator);
        assertEquals(d3, listOfDrivers.get(0));
        assertEquals(d2, listOfDrivers.get(4));

        System.out.println("Sorting by name : " + listOfDrivers);

        Collections.sort(listOfDrivers, Driver.addressComparator);
        assertEquals(d2, listOfDrivers.get(0));
        assertEquals(d1, listOfDrivers.get(4));

        System.out.println("Sorting by address : " + listOfDrivers);

        Collections.sort(listOfDrivers, Driver.phoneComparator);
        assertEquals(d3, listOfDrivers.get(0));
        assertEquals(d2, listOfDrivers.get(4));

        System.out.println("Sorting by phone : " + listOfDrivers);

        Collections.sort(listOfDrivers, Driver.licenseComparator);
        assertEquals(d3, listOfDrivers.get(0));
        assertEquals(d2, listOfDrivers.get(4));

        System.out.println("Sorting by license : " + listOfDrivers);

    }
}
