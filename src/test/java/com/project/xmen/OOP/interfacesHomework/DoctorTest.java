package com.project.xmen.OOP.interfacesHomework;

import com.project.xmen.OOP.InterfacesHomework.Doctor;
import com.project.xmen.interfaces.Employee;
import org.junit.Test;

import javax.print.Doc;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import static junit.framework.TestCase.assertEquals;

public class DoctorTest {

    @Test
    public void testDoctorSorting(){

        Doctor d1 = new Doctor("Leo", "Yangelay str.", "+380991237380", 3, "aab", 1500,1);
        Doctor d2 = new Doctor("Tom", "Bilinskogo str.", "+380992235980", 1, "aab", 1300,3);
        Doctor d3 = new Doctor("Bob", "Karalenko str.", "+380550128738", 4, "aab", 1450,2);
        Doctor d4 = new Doctor("Tolay", "Shevchenko str.", "+380991237380", 5, "aab", 11100,1);
        Doctor d5 = new Doctor("Max", "Kulbesheva str.", "+380661212389", 2, "aab", 4300,2);

        List<Doctor> listOfDoctors = new ArrayList<>();

        listOfDoctors.add(d1);
        listOfDoctors.add(d2);
        listOfDoctors.add(d3);
        listOfDoctors.add(d4);
        listOfDoctors.add(d5);
        System.out.println("Unsorted List : " + listOfDoctors);

        Collections.sort(listOfDoctors);
        assertEquals(d2, listOfDoctors.get(0));
        assertEquals(d4, listOfDoctors.get(4));

        System.out.println("Sorted list : " + listOfDoctors);

        Collections.sort(listOfDoctors, Doctor.nameComparator);
        assertEquals(d3, listOfDoctors.get(0));
        assertEquals(d2, listOfDoctors.get(4));

        System.out.println("Sorting by name : " + listOfDoctors);

        Collections.sort(listOfDoctors, Doctor.addressComparator);
        assertEquals(d2, listOfDoctors.get(0));
        assertEquals(d1, listOfDoctors.get(4));

        System.out.println("Sorting by address : " + listOfDoctors);

        Collections.sort(listOfDoctors, Doctor.phoneComparator);
        assertEquals(d3, listOfDoctors.get(0));
        assertEquals(d2, listOfDoctors.get(4));

        System.out.println("Sorting by phone : " + listOfDoctors);

        Collections.sort(listOfDoctors, Doctor.degreeComparator);
        assertEquals(d4, listOfDoctors.get(0));
        assertEquals(d2, listOfDoctors.get(4));

        System.out.println("Sorting by doctorDegree : " + listOfDoctors);


    }
}
