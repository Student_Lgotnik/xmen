package com.project.xmen.OOP.interfacesHomework;

import com.project.xmen.OOP.InterfacesHomework.Seller;
import com.project.xmen.OOP.InterfacesHomework.Tutor;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static junit.framework.TestCase.assertEquals;

public class TutorTest {

    @Test
    public void testSellerSorting(){

        Tutor s1 = new Tutor("Leo", "Yangelay str.", "+380991237380",  3, "3l16", 5600, 40);
        Tutor s2 = new Tutor("Tom", "Bilinskogo str.", "+380992235980", 1, "3l16", 5600, 40);
        Tutor s3 = new Tutor("Bob", "Karalenko str.", "+380550128738", 4, "3l16", 5600, 40);
        Tutor s4 = new Tutor("Tolay", "Shevchenko str.", "+380991237380", 5, "3l16", 5600, 40);
        Tutor s5 = new Tutor("Max", "Kulbesheva str.", "+380661212389", 2, "3l16", 5600, 40);

        List<Tutor> listOfTutors = new ArrayList<>();

        listOfTutors.add(s1);
        listOfTutors.add(s2);
        listOfTutors.add(s3);
        listOfTutors.add(s4);
        listOfTutors.add(s5);
        System.out.println("Unsorted List : " + listOfTutors);

        Collections.sort(listOfTutors);
        assertEquals(s2, listOfTutors.get(0));
        assertEquals(s4, listOfTutors.get(4));

        System.out.println("Sorted list : " + listOfTutors);

        Collections.sort(listOfTutors, Tutor.nameComparator);
        assertEquals(s3, listOfTutors.get(0));
        assertEquals(s2, listOfTutors.get(4));

        System.out.println("Sorting by name : " + listOfTutors);

        Collections.sort(listOfTutors, Tutor.addressComparator);
        assertEquals(s2, listOfTutors.get(0));
        assertEquals(s1, listOfTutors.get(4));

        System.out.println("Sorting by address : " + listOfTutors);

        Collections.sort(listOfTutors, Tutor.phoneComparator);
        assertEquals(s3, listOfTutors.get(0));
        assertEquals(s2, listOfTutors.get(4));

        System.out.println("Sorting by phone : " + listOfTutors);

    }

}
