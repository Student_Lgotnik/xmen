package com.project.xmen.OOP.interfacesHomework;

import com.project.xmen.OOP.InterfacesHomework.Seller;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static junit.framework.TestCase.assertEquals;

public class SellerTest {

    @Test
    public void testSellerSorting(){

        Seller s1 = new Seller("Leo", "Yangelay str.", "+380991237380", 3,"3l16", 5600,"Leo license");
        Seller s2 = new Seller("Tom", "Bilinskogo str.", "+380992235980", 1, "3l16", 5700,"Tom license");
        Seller s3 = new Seller("Bob", "Karalenko str.", "+380550128738", 4, "3l16", 5800,"Bob license");
        Seller s4 = new Seller("Tolay", "Shevchenko str.", "+380991237380", 5, "3l16", 4900,"Tolay license");
        Seller s5 = new Seller("Max", "Kulbesheva str.", "+380661212389", 2, "3l16", 7200,"Max license");

        List<Seller> listOfSellers = new ArrayList<>();

        listOfSellers.add(s1);
        listOfSellers.add(s2);
        listOfSellers.add(s3);
        listOfSellers.add(s4);
        listOfSellers.add(s5);
        System.out.println("Unsorted List : " + listOfSellers);

        Collections.sort(listOfSellers);
        assertEquals(s2, listOfSellers.get(0));
        assertEquals(s4, listOfSellers.get(4));

        System.out.println("Sorted list : " + listOfSellers);

        Collections.sort(listOfSellers, Seller.nameComparator);
        assertEquals(s3, listOfSellers.get(0));
        assertEquals(s2, listOfSellers.get(4));

        System.out.println("Sorting by name : " + listOfSellers);

        Collections.sort(listOfSellers, Seller.addressComparator);
        assertEquals(s2, listOfSellers.get(0));
        assertEquals(s1, listOfSellers.get(4));

        System.out.println("Sorting by address : " + listOfSellers);

        Collections.sort(listOfSellers, Seller.phoneComparator);
        assertEquals(s3, listOfSellers.get(0));
        assertEquals(s2, listOfSellers.get(4));

        System.out.println("Sorting by phone : " + listOfSellers);

        Collections.sort(listOfSellers, Seller.licenseComparator);
        assertEquals(s3, listOfSellers.get(0));
        assertEquals(s2, listOfSellers.get(4));

        System.out.println("Sorting by doctorDegree : " + listOfSellers);


    }

}
