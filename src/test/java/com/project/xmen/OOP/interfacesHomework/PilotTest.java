package com.project.xmen.OOP.interfacesHomework;

import com.project.xmen.OOP.InterfacesHomework.Doctor;
import com.project.xmen.OOP.InterfacesHomework.Pilot;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static junit.framework.TestCase.assertEquals;

public class PilotTest {

    @Test
    public void testPilotSorting(){

        Pilot p1 = new Pilot("Leo", "Yangelay str.", "+380991237380", 3,"9z56", 5600, 54);
        Pilot p2 = new Pilot("Tom", "Bilinskogo str.", "+380992235980", 1, "9z56", 6500,31);
        Pilot p3 = new Pilot("Bob", "Karalenko str.", "+380550128738", 4,"9z56", 7100, 12);
        Pilot p4 = new Pilot("Tolay", "Shevchenko str.", "+380991237380", 5, "9z56", 6100,11);
        Pilot p5 = new Pilot("Max", "Kulbesheva str.", "+380661212389", 2, "9z56", 6600,174);

        List<Pilot> listOfPilots = new ArrayList<>();

        listOfPilots.add(p1);
        listOfPilots.add(p2);
        listOfPilots.add(p3);
        listOfPilots.add(p4);
        listOfPilots.add(p5);
        System.out.println("Unsorted List : " + listOfPilots);

        Collections.sort(listOfPilots);
        assertEquals(p2, listOfPilots.get(0));
        assertEquals(p4, listOfPilots.get(4));

        System.out.println("Sorted list : " + listOfPilots);

        Collections.sort(listOfPilots, Pilot.nameComparator);
        assertEquals(p3, listOfPilots.get(0));
        assertEquals(p2, listOfPilots.get(4));

        System.out.println("Sorting by name : " + listOfPilots);

        Collections.sort(listOfPilots, Pilot.addressComparator);
        assertEquals(p2, listOfPilots.get(0));
        assertEquals(p1, listOfPilots.get(4));

        System.out.println("Sorting by address : " + listOfPilots);

        Collections.sort(listOfPilots, Pilot.phoneComparator);
        assertEquals(p3, listOfPilots.get(0));
        assertEquals(p2, listOfPilots.get(4));

        System.out.println("Sorting by phone : " + listOfPilots);

        Collections.sort(listOfPilots, Pilot.militaryComparator);
        assertEquals(p4, listOfPilots.get(0));
        assertEquals(p5, listOfPilots.get(4));

        System.out.println("Sorting by doctorDegree : " + listOfPilots);


    }
}
