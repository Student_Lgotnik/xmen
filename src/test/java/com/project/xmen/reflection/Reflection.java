package com.project.xmen.reflection;

import com.project.xmen.annotations.People;
import org.junit.Test;

import java.lang.annotation.Annotation;
import java.lang.reflect.Array;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.Arrays;

public class Reflection {
    @Test
    public void reflection() {
        People people = new People();
        Class myClass = people.getClass();

        Class mySeccondClass = People.class;

        System.out.println(Arrays.toString(people.getClass().getFields()));
        System.out.println(people.getClass().getModifiers());
        System.out.println(Arrays.toString(people.getClass().getMethods()));

        Package pack = people.getClass().getPackage();
        System.out.println(pack.getName());

        Class[] interfaces = myClass.getInterfaces();
        Annotation[] annotationsClass = myClass.getAnnotations();
        for (int i = 0; i < interfaces.length; i++) {
            System.out.println(i == 0 ? "implements" : ", ");
            System.out.print(interfaces[i].getSimpleName());
        }

        Arrays.asList(annotationsClass).forEach(annotation
                -> System.out.println("@" + annotation.annotationType().getSimpleName()));
        System.out.println(" {");

        Field[] fields = myClass.getDeclaredFields();
        for (Field field : fields){
            System.out.println("\t" + field.getModifiers() + " " + field.getType() + " " + field.getName() + ";");
        }

        Method[] methods = myClass.getDeclaredMethods();
        for (Method m : methods) {
            Annotation[] annotations = m.getAnnotations();
            System.out.println("\t");
            for (Annotation a : annotations) {
                System.out.println("@" + a.annotationType().getSimpleName() + " ");
            }
            System.out.println();
            System.out.print("\t" + m.getModifiers() + " " + m.getReturnType() + " " + m.getName() + ";");
        }


    }





}
