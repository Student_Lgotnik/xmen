package com.project.xmen.regex;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class FileValidatorTest {

    private FileValidator fileValidator;
    final static String UNVALIDATE_PATH = "/home/student/HIllel/classwork/xmen/src/main/resources/UnvalidateFiles";
    final static String VALIDATE_PATH = "/home/student/HIllel/classwork/xmen/src/main/resources/ValidateFiles";

    @BeforeClass
    public void initData(){
        fileValidator = new FileValidator();
    }


    @Test
    public void test() {
        validateFolder(UNVALIDATE_PATH);
    }

    public void validateFolder(String unvalidatePath) {
        try (Stream<Path> paths = Files.walk(Paths.get(unvalidatePath))) {
            File f = new File(VALIDATE_PATH);
            if (!f.exists())
                Files.createDirectories(Paths.get(VALIDATE_PATH));
            List<Path> files = paths.map(Path::getFileName).collect(Collectors.toList());
            for (int i = 1; i < files.size(); i++){
                try(OutputStream os = new FileOutputStream(VALIDATE_PATH + "/" + fileValidator.validate(files.get(i).toString()))){
                    Files.copy(Paths.get(unvalidatePath + "/" + files.get(i).toString()), os);
                }
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @org.junit.Test
    public void copy() {
        Path oldFile = Paths.get("/home/student/HIllel/classwork/xmen/","paths.txt");
        Path newFile = Paths.get("src/main/resources", "newFile.txt");
        try(OutputStream os = new FileOutputStream(newFile.toFile())) {
            Files.copy(oldFile, os);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
