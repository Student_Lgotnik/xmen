package com.project.xmen.javaeight.homework;

import java.util.List;
import java.util.Objects;

public class Vegetable {

    private int id;
    private String name;
    private String colour;
    private int price;

    public Vegetable() {
    }

    public static List<Vegetable> clearList(List<Vegetable> vegetables) {
        for (int i = 0; i < vegetables.size(); i++) {
            Vegetable v = vegetables.get(i);
            for (int j = i+1; j < vegetables.size(); j++) {
                if (v.equals(vegetables.get(j))){
                    vegetables.remove(j);
                    vegetables.remove(v);
                }
            }
        }
        return vegetables;
    }


    public Vegetable(int id, String name, String colour, int price) {
        this.id = id;
        this.name = name;
        this.colour = colour;
        this.price = price;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setColour(String colour) {
        this.colour = colour;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getColour() {
        return colour;
    }

    public int getPrice() {
        return price;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Vegetable vegetable = (Vegetable) o;
        return id == vegetable.id &&
                price == vegetable.price &&
                Objects.equals(name, vegetable.name) &&
                Objects.equals(colour, vegetable.colour);
    }

    @Override
    public int hashCode() {

        return Objects.hash(id, name, colour, price);
    }

    @Override
    public String toString() {
        return "Vegetable{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", colour='" + colour + '\'' +
                ", price=" + price +
                '}';
    }
}
