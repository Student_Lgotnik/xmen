package com.project.xmen.interfaces;

public interface SoundRegulator {

    void up();
    void down();

}
