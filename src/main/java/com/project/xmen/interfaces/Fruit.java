package com.project.xmen.interfaces;

import java.util.Comparator;

public class Fruit implements Comparable<Fruit>{

    private String fruitName;
    private String fruitDesc;
    private int quantity;

    public Fruit(String fruitName, String fruitDesc, int quantity) {
        this.fruitName = fruitName;
        this.fruitDesc = fruitDesc;
        this.quantity = quantity;
    }

    public void setFruitName(String fruitName) {
        this.fruitName = fruitName;
    }

    public void setFruitDesc(String fruitDesc) {
        this.fruitDesc = fruitDesc;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public String getFruitName() {
        return fruitName;
    }

    public String getFruitDesc() {
        return fruitDesc;
    }

    public int getQuantity() {
        return quantity;
    }

    @Override
    public int compareTo(Fruit compareFruit) {

        int compareQuantity = compareFruit.getQuantity();

        //return this.quantity - compareQuantity;

        return compareQuantity - this.quantity;

    }

    public static Comparator<Fruit> fruitComparator = new Comparator<Fruit>() {
        @Override
        public int compare(Fruit fruit1, Fruit fruit2) {

            String fruitName1 = fruit1.getFruitName().toUpperCase();
            String fruitName2 = fruit2.getFruitName().toUpperCase();

            //return fruitName1.compareTo(fruitName2);

            return fruitName2.compareTo(fruitName1);
        }
    };
}
