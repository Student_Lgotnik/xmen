package com.project.xmen.interfaces;

public interface ButtonSwitch {

    void switchON();
    void switchOFF();

}
