package com.project.xmen.jsoup;

import org.jsoup.Connection;
import org.jsoup.Jsoup;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;

public class ThreadsController implements Runnable{

    private String url;
    private int counter;
    private Path path = Paths.get(".").toAbsolutePath().getParent();
    private static boolean DEBUG = false;

    public ThreadsController(String url){
        this.url = url;
    }


    @Override
    public void run() {

        try {
            File linksFile = new File(path + "/src/main/resources/links", Thread.currentThread().getName() + ".html");
            try(BufferedWriter linksFileWriter = new BufferedWriter(new FileWriter(linksFile, true))){

                Connection.Response html = Jsoup.connect(url).execute();
                linksFileWriter.write(html.body());

                if (DEBUG = true);
                System.out.println(Thread.currentThread().getName() + "html");

                counter = TestMain.atomicInteger.incrementAndGet();
                System.out.println(counter + " number of Sites are checked!");

            }
        }  catch (IOException e) {
            e.printStackTrace();
        }

    }
}
