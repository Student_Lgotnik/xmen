package com.project.xmen.inner;

import java.util.HashMap;
import java.util.Map;

public class Cashe {

    private Map<String, CasheEntry> casheMap = new HashMap<>();

    private class CasheEntry {

        public Long timeInserted = 0l;
        public Object value = null;

        @Override
        public String toString() {
            return "CasheEntry{" +
                    "timeInserted=" + timeInserted +
                    ", value=" + value +
                    '}';
        }
    }

    public void store(String key, Object value){

        CasheEntry entry = new CasheEntry();
        entry.value = value;
        entry.timeInserted = System.currentTimeMillis();
        this.casheMap.put(key, entry);
        TimeOver timeOver = new TimeOver();
        Thread thread = new Thread(timeOver);
        thread.start();
    }


    public Object get(String key) {
        CasheEntry entry = this.casheMap.get(key);
        if (entry == null) return null;
        return entry.value;
    }

    public String getData(String key) {
        CasheEntry entry = this.casheMap.get(key);
        if (entry == null) return null;
        return entry.value.toString().concat(" ").concat(entry.timeInserted.toString());
    }

    class TimeOver implements Runnable {

        @Override
        public void run() {
            int lifetime = 30000;

            while (true) {

                if (casheMap.isEmpty()) {
                    break;
                }

                for (Map.Entry<String, CasheEntry> entry: casheMap.entrySet()){
                    //System.out.println(entry);
                    Long current = System.currentTimeMillis();
                    Long c = current - entry.getValue().timeInserted;
                    if (c > lifetime){
                        casheMap.remove(entry.getKey());
                    }
                }
            }
        }

    }

}
