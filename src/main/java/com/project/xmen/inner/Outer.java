package com.project.xmen.inner;

import java.util.Locale;

public class Outer {

    private static String textNested = "Nested String!";
    private String text = "I am private!";

    //Nested class может испольховать статические, публичные, приватные методы
    //Но принимает только статические переменные
    public static class Nested {

        public void printNestedText(){
            System.out.println(textNested);
        }

        public static void printStaticNestedText(){
            System.out.println(textNested);
        }
    }

    //Inner class не может исполльзовать статические методы
    //Но может использовать статические, публичные, приватные переменные
    public class Inner {

        private String text = "I am Inner private!";

        public void printText() {
            System.out.println(text);
            System.out.println(Outer.this.text);
        }

        public void printStaticField(){
            System.out.println(textNested);
        }

    }

    public void printClasses() {
        Outer.Nested nested = new Outer.Nested();
        nested.printNestedText();
        Outer.Nested.printStaticNestedText();

        Inner inner = new Inner();
        inner.printText();
        inner.printStaticField();
    }

    public void local() {

        //существует только в пределах этого метода
        class Local {

            int x;

            public Local(int x) {
                this.x = x;
            }

            public Local() {
                x = 5;
            }
            int getX() {
                return this.x;
            }

            Local getCurrentLocal() {
                return this;
            }

            Local getNewLocal() {
                return new Local();
            }

        }

        Local local  = new Local(10);
        System.out.println(local.getX());
        System.out.println(local.getNewLocal().getX());
        System.out.println(local.getCurrentLocal().getX());
    }

    public static void main(String[] args) {
        Outer outer = new Outer();
        outer.local();
    }

    public void doIt() {
        System.out.println("Do something!");
    }

}
