package com.project.xmen.annotations;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.Serializable;

@Component
public class People  implements Serializable, Cloneable{

    private String name;
    private int age;
    public int sum;
    protected int test;

    protected String getName() {
        return this.name;
    }

    public int getAge() {
        return age;
    }

    public int getSum() {
        return sum;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public void setSum(int sum) {
        this.sum = sum;
    }

    private void test() {

    }

    private void test2() {

    }

    @Autowired
    @Deprecated
    protected static void method(String[] params) { }

    @Autowired
    @Override
    public String toString() {
        return super.toString();
    }
}
