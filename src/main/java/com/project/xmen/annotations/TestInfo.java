package com.project.xmen.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
public @interface TestInfo {

    enum Prioriry {
        LOW, MEDIUM, HIGH
    }

    Prioriry priority() default Prioriry.MEDIUM;

    String[] tags() default "";

    String createdBy() default "Maxim";

    String lastModified() default "02/04/2018";

}
