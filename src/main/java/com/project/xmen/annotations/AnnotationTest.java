package com.project.xmen.annotations;

@TestInfo(
        priority = TestInfo.Prioriry.HIGH,
        createdBy = "Andrey",
        tags = {"sales", "test"}
)
public class AnnotationTest {

    @Test
    void testA() {
        if (true)
            throw new RuntimeException("This test always failed");
    }

    @Test(enabled = false)
    void testB() {
        if (false)
            throw new RuntimeException("This test always passed");
    }

    @Test
    void testC() {
        if (10>1){
            //Do nothing
        }
    }

}
