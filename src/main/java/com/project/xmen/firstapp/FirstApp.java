package com.project.xmen.firstapp;

import static java.lang.System.*;

public class FirstApp {

    public FirstApp(){}

    /**
     * Simple method print
     * @param t
     * @param <T>
     */
    public static <T> void print(T t){
        out.print(t);
    }

    /**
     * Print object
     * @param object
     */
    public static void println(Object object){
        out.println(object);
    }

    /**
     * Create new line
     */
    public static void println(){
        out.println();
    }
}
