package com.project.xmen.generics;

public class Dog extends Animal{

    private String dogName;


    @Override
    Dog name(Object o) {
        this.dogName = (String)o;
        return this;
    }

    @Override
    Object getData() {
        return dogName;
    }
}
