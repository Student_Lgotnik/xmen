package com.project.xmen.maps;

public class User {

    private String firsName;
    private String lastName;
    private int salary;


    public User(String firsName, String lastName, int salary) {
        this.firsName = firsName;
        this.lastName = lastName;
        this.salary = salary;
    }


    public void setFirsName(String firsName) {
        this.firsName = firsName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public void setSalary(int salary) {
        this.salary = salary;
    }

    public String getFirsName() {

        return firsName;
    }

    public String getLastName() {
        return lastName;
    }

    public int getSalary() {
        return salary;
    }

    @Override
    public String toString() {
        return "User{" +
                "firsName='" + firsName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", salary=" + salary +
                '}';
    }
}
