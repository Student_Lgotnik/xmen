package com.project.xmen.maps;

import java.util.*;

public class TreeMapExample {

    public static void main(String[] args) {
        Map<String, String> treeMap = new TreeMap<>();
        treeMap.put("Bruce", "Willis");
        treeMap.put("Arnold", "Schwarz");
        treeMap.put("Jackie", "Chan");
        treeMap.put("Sylvester", "Stallone");
        treeMap.put("Chuck", "Norris");

        for(Map.Entry tree : treeMap.entrySet()){
            System.out.println(tree.getKey()+" "+ tree.getValue());
        }

        // Create a tree map
        TreeMap<String, Double> tradeMarkMap = new TreeMap<>();

        // Put elements to the map
        tradeMarkMap.put("Zara", 8284.37);
        tradeMarkMap.put("Bershka", 354.11);
        tradeMarkMap.put("Cropp", 2363.00);
        tradeMarkMap.put("Stradivarius", 78.12);
        tradeMarkMap.put("Pull & Bear", -25.34);

        // Lambda Example
        tradeMarkMap.forEach((key, value) -> {
            System.out.println(key + " = " + value);
            System.out.println();
        });

        System.out.println("-----------------------------");

        Set set = tradeMarkMap.entrySet();

        Iterator i = set.iterator();

        while (i.hasNext()){
            Map.Entry entryMap = (Map.Entry)i.next();
            System.out.print(entryMap.getValue() + ": ");
            System.out.println(entryMap.getValue());
        }
        System.out.println();

        double balance = tradeMarkMap.get("Zara");
        tradeMarkMap.put("Zara", balance + 1000);
        System.out.println("Zara's new balance: " + tradeMarkMap.get("Zara"));

        Map<User, String> userMap = new TreeMap<>(new UserSalaryComparator());

        userMap.put(new User("Nil", "Tompson", 9826), "My name is 1");
        userMap.put(new User("Deryl", "Logan", 45986), "My name is 2");
        userMap.put(new User("Andriy", "Shevchenko", 96), "My name is 3");
        userMap.put(new User("Pedro", "Gonzales", 9), "My name is 4");


        System.out.println(userMap.toString());

        Set keySet = userMap.entrySet();
        for (Object user : keySet){
            System.out.println(user);
        }

        compareCars(Car.idComparator);
        compareCars(Car.modelComparator);
        compareCars(Car.tradeMarkComparator);
        compareCars(Car.yearComparator);

    }

    private static void compareCars(Comparator<Car> carComp) {

        Map<Car, Integer> carMap = new TreeMap<>(carComp);

        carMap.put(new Car(1, "BMW", "M6", 2008), 28000);
        carMap.put(new Car(2, "Audi", "Q7", 2011), 35000);
        carMap.put(new Car(3, "Volkswagen", "Passat", 2008), 28000);
        carMap.put(new Car(4, "Mitsubishi", "Pajero", 2007), 19000);
        carMap.put(new Car(5, "Nissan", "Juke", 2001), 11000);

        carMap.forEach((key, value) -> {
            System.out.println(key + " = " + value);
            System.out.println();
        });

        System.out.println("------------------------------");

    }
}
