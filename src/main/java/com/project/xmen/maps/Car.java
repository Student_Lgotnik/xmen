package com.project.xmen.maps;

import java.util.Comparator;

public class Car implements Comparable<Car>{

    private int id;
    private String trademark;
    private String model;
    private int year;

    public Car(int id, String trademark, String model, int year) {
        this.id = id;
        this.trademark = trademark;
        this.model = model;
        this.year = year;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setTrademark(String trademark) {
        this.trademark = trademark;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public int getId() {

        return id;
    }

    public String getTrademark() {
        return trademark;
    }

    public String getModel() {
        return model;
    }

    public int getYear() {
        return year;
    }

    @Override
    public String toString() {
        return "Car{" +
                "id=" + id +
                ", trademark='" + trademark + '\'' +
                ", model='" + model + '\'' +
                ", year=" + year +
                '}';
    }

    public static Comparator<Car> idComparator = new Comparator<Car>() {
        @Override
        public int compare(Car car1, Car car2) {
            return car1.id - car2.id;
        }
    };


    public static Comparator<Car> tradeMarkComparator = new Comparator<Car>() {
        @Override
        public int compare(Car car1, Car car2) {
            return car1.trademark.compareTo(car2.trademark);
        }
    };

    public static Comparator<Car> modelComparator = new Comparator<Car>() {
        @Override
        public int compare(Car car1, Car car2) {
            return car1.model.compareTo(car2.model);
        }
    };

    public static Comparator<Car> yearComparator = new Comparator<Car>() {
        @Override
        public int compare(Car car1, Car car2) {
            return car1.year - car2.year;
        }
    };

    @Override
    public int compareTo(Car o) {
        return 0;
    }
}
