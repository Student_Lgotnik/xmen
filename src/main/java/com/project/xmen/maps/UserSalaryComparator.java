package com.project.xmen.maps;

import java.util.Comparator;

public class UserSalaryComparator implements Comparator<User>{


    @Override
    public int compare(User u1, User u2) {
        if (u1.getSalary() >= u2.getSalary()){
            return 1;
        } else return -1;
    }
}
