package com.project.xmen.regex;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class FileValidator {
    private Pattern pattern;
    private Matcher matcher;

    /**
     *    ^ - Start of the line
     *   [a-z0-9_-] - Match characters and symbols in the list, a-z, 0-9, underscore, hyphen
     *   {3,15}  - Length at least 3 characters and maximum length of 15
     *   $  - End of the line
     */
    private static final String FILE_PATTERN = "^[a-z0-9*_-]{3,15}$";

    public FileValidator() {
        pattern = Pattern.compile(FILE_PATTERN);
    }

    /**
     * Validate username with regular expression
     * @param filename username for validation
     * @return true valid username, false invalid username
     */
    public String validate(String filename) {
        matcher = pattern.matcher(filename);
        StringBuilder sb = new StringBuilder(filename);
        if (matcher.matches())
            return filename;
        else {
            if (sb.length() > 15){
                sb.replace(0, sb.length() - 1, sb.substring(0, 14));
            }
            while (sb.length() < 3)
                sb.append("*");
            while (firstFailurePoint(pattern, sb.toString()) != -1){
                int failure = firstFailurePoint(pattern, sb.toString());
                sb.replace(failure, failure + 1, "*");

            }
        }
        return sb.toString();
    }

    public static int firstFailurePoint(Pattern regex, String str) {
        for (int i = 0; i <= str.length(); i++) {
            Matcher m = regex.matcher(str.substring(0, i));
            if (!m.matches() && !m.hitEnd()) {
                return i - 1;
            }
        }
        if (regex.matcher(str).matches()) {
            return -1;
        } else {
            return str.length();
        }
    }

}
