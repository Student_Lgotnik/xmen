package com.project.xmen.exceptions;
import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import java.util.zip.ZipOutputStream;

public class AppZip {

    List<String> fileList;
    private static final String OUTPUT_ZIP_FILE = "/home/student/HIllel/classwork/xmen/src/main/resources/MyZIP.zip";
    private static final String SOURCE_FOLDER = "/home/student/HIllel/classwork/xmen/src/main/resources";
    private static final String UNZIPED_FOLDER = "/home/student/HIllel/classwork/xmen/src/main/resources/UnzipedFiles";

    public AppZip() {
        fileList = new ArrayList<>();
    }


    public static void main(String[] args) {
        AppZip appZip = new AppZip();
//        appZip.generateFileList(new File(SOURCE_FOLDER));
//        appZip.zipIt(OUTPUT_ZIP_FILE);
        appZip.unzipIt(OUTPUT_ZIP_FILE,UNZIPED_FOLDER);
    }

    public void generateFileList(File node) {
        if (node.isFile()){
            fileList.add(generateZipEntry(node.getAbsoluteFile().toString()));
        }
        if (node.isDirectory()){
            String[] subNote = node.list();
            for (String filename : subNote) {
                generateFileList(new File(node, filename));
            }
        }
    }

    private String generateZipEntry(String file){
        return file.substring(SOURCE_FOLDER.length()+1, file.length());
    }

    public void zipIt(String zipFile) {

        byte[] buffer = new byte[1024];

        try (FileOutputStream fos = new FileOutputStream(zipFile);
             ZipOutputStream zos = new ZipOutputStream(fos)){

            System.out.println("Output to zip : " + zipFile);

            for (String file : this.fileList){

                ZipEntry ze = new ZipEntry(file);
                zos.putNextEntry(ze);

                try (FileInputStream in = new FileInputStream(SOURCE_FOLDER + File.separator  + file)){

                    int len;

                    while ((len = in.read(buffer)) > 0){
                        zos.write(buffer, 0 , len);
                    }
                    System.out.println("File Added : " + file);
                }

            }

            fos.flush();
            zos.closeEntry();

            System.out.println("Done");

        }  catch (IOException e) {
            e.printStackTrace();
        }

    }

    public void unzipIt(String zipFile, String unzipFolder) {

        byte[] buffer = new byte[1024];

        try (FileInputStream fin = new FileInputStream(zipFile);
             ZipInputStream zis = new ZipInputStream(fin)){

            ZipEntry zipEntry = zis.getNextEntry();

            while(zipEntry != null){
                String fileName = zipEntry.getName();
                File newFile = new File(unzipFolder + File.separator + fileName);
                try (FileOutputStream fos = new FileOutputStream(newFile)){
                    int len;
                    while ((len = zis.read(buffer)) > 0) {
                        fos.write(buffer, 0, len);
                    }
                    zipEntry = zis.getNextEntry();
                }

            }
            zis.closeEntry();
            zis.close();


        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

}
