package com.project.xmen.exceptions;

public class NameNotFoundException extends Exception {

    private int errCode;

    public NameNotFoundException(int errCode, String messege) {
        super(messege);
        this.errCode = errCode;
    }

    public void setErrCode(int errCode) {
        this.errCode = errCode;
    }

    public int getErrCode() {

        return errCode;
    }
}
