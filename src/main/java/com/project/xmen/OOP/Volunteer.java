package com.project.xmen.OOP;

public class Volunteer extends StaffMember {

    /**
     * Information about volonteer
     * @param eName
     * @param eAddress
     * @param ePhone
     */
    public Volunteer(String eName, String eAddress, String ePhone){
        super(eName, eAddress, ePhone);
    }

    /**
     * Return zero for volonteer
     * @return
     */
    @Override
    public double pay() {
        return 0.0;
    }
}
