package com.project.xmen.OOP;

import java.util.Date;

public class User {

    private String firsName;
    private String lastName;
    private String loginId;
    private String password;
    private String email;
    private Date registered;

    public void setFirsName(String firsName) {
        this.firsName = firsName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

//    public void setLoginId(String loginId) {
//        this.loginId = loginId;
//    }

    public void setLoginId(String loginId) {
        if (loginId.matches("^[a-zA-Z0-9]*$")) {
            this.loginId = loginId;
        } else {
            System.err.println("Error!");
        }
    }

    public String getFirsName() {
        return firsName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getLoginId() {
        return loginId;
    }

//    public void setPassword(String password){
//        if (password.length() < 6){
//            System.out.println("Password is too short");
//        }
//        else {
//            this.password = password;
//        }
//    }

    public void setPassword(String password){
        if (password == null || password.length() < 6){
            System.out.println("Password is too short");
        }
        else {
            this.password = password;
        }
    }

    public String getEmail() {
        return email;
    }

    public String getPassword() {
        return password;
    }

    public Date getRegistered() {
        return registered;
    }

    public void setEmail(String email) {

        this.email = email;
    }

    public void setRegistered(Date registered) {
        this.registered = registered;
    }
}
