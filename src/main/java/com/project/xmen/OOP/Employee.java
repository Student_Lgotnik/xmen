package com.project.xmen.OOP;

import com.project.xmen.OOP.InterfacesHomework.Paid;

public class Employee extends StaffMember implements Paid{

    private int id;
    private String socialSecurityNumber;
    private double payRate;

    public Employee(String name, String address, String phone, int id, String socialSecurityNumber, double payRate) {
        super(name, address, phone);
        this.id = id;
        this.socialSecurityNumber = socialSecurityNumber;
        this.payRate = payRate;
    }

    @Override
    public String toString() {
        String result = super.toString();
        result += "\nSocial Security Number: " + socialSecurityNumber;
        return result;
    }


    public int getId() {
        return id;
    }

    public String getSocialSecurityNumber() {
        return socialSecurityNumber;
    }

    public double getPayRate() {
        return payRate;
    }

    @Override
    public double pay() { return payRate; }
}
