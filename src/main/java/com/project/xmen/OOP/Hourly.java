package com.project.xmen.OOP;

public class Hourly extends Employee{

    private int hoursWorked;

    /**
     *
     * @param name
     * @param address
     * @param phone
     * @param id
     * @param socialSecurityNumber
     * @param payRate
     * @param hoursWorked
     */
    public Hourly(String name, String address, String phone, int id, String socialSecurityNumber, double payRate, int hoursWorked) {
        super(name, address, phone, id, socialSecurityNumber, payRate);
        this.hoursWorked = hoursWorked;
    }

    /**
     * Add more hours
     * @param moreHours
     */
    public void addHours(int moreHours){
        hoursWorked += moreHours;
    }

    /**
     * Return pay for hourly employee
     * @return
     */
    @Override
    public double pay() {
        double payment = super.pay() + hoursWorked;
        hoursWorked = 0;
        return payment;
    }

    /**
     * Returns infom about hourly employee
     * @return
     */
    @Override
    public String toString() {
       String result = super.toString();
       result += "\nCurrent hours: " + hoursWorked;
       return result;
    }

}
