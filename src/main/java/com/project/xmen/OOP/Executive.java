package com.project.xmen.OOP;

public class Executive extends Employee {

    private double bonus;


    /**
     *
     * @param name
     * @param address
     * @param phone
     * @param id
     * @param socialSecurityNumber
     * @param payRate
     * @param bonus
     */
    public Executive(String name, String address, String phone, int id, String socialSecurityNumber, double payRate, double bonus) {
        super(name, address, phone, id, socialSecurityNumber, payRate);
        this.bonus = bonus;
    }

    /**
     * Awards the specified bonus to this executive.
     * @param execBonus
     */
    public void awarBonus (double execBonus){
        bonus = execBonus;
    }

    /**
     * Computes and returns the pay for an executive, with is regular employee paument plus a one-time bonus.
     * @return
     */
    @Override
    public double pay() {
        double payment = super.pay() + bonus;
        bonus = 0;
        return payment;
    }
}
