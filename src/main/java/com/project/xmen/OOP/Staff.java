package com.project.xmen.OOP;

import java.util.ArrayList;

public class Staff {

    ArrayList<StaffMember> staffList;

    public Staff(){
        staffList = new ArrayList<>();
        staffList.add(new Executive("Sam", "123 Main line", "555-0469", 4,"123-45-6789", 2423.07, 500));
        staffList.add(new Employee("Carla", "456 off line", "555-1010", 3, "789-11-6789", 1246.15));
        staffList.add(new Employee("Moody", "789 off Rocket", "555-0000", 1, "010-25-4889", 1169.23));
        staffList.add(new Hourly("Diane", "678 First Ave.", "555-0690", 2,"958-20-3040", 10.55, 16));
        staffList.add(new Volunteer("Norm", "987 Suds Blvd.", "555-8374"));
        staffList.add(new Volunteer("Cliff", "432 Duds lane", "555-7282"));

        for (StaffMember sm : staffList) {
            if (sm instanceof Executive)
                ((Executive) sm).awarBonus(500.00);
            else if (sm instanceof Hourly)
                ((Hourly) sm).addHours(40);
        }
    }

    /**
     * Pays all staff members.
     */
    public void payday (){
        double amount;
        for (int count = 0; count < staffList.size(); count++){
            System.out.println(staffList.get(count));
            amount = staffList.get(count).pay();

            if (amount == 0.0){
                System.out.println("Thanks!");
            } else {
                System.out.println("Paind: " + amount);
            }
            System.out.println("-----------------------");
        }
    }

}
