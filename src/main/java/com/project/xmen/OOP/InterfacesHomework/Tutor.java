package com.project.xmen.OOP.InterfacesHomework;

import com.project.xmen.OOP.Employee;
import com.project.xmen.OOP.Hourly;
import com.project.xmen.OOP.StaffMember;

import java.util.Comparator;

public class Tutor extends Hourly implements FlexibleWork, NeedHighEd, Comparable<Tutor> {


    public Tutor(String name, String address, String phone, int id, String socialSecurityNumber, double payRate, int hoursWorked) {
        super(name, address, phone, id, socialSecurityNumber, payRate, hoursWorked);
    }

    @Override
    public void startFlex() {
        System.out.println("You can start working anytime!");
    }

    @Override
    public void showDiploma(String nameOfUniversity) {
        System.out.println("I graduated from the " + nameOfUniversity + " university!");
    }

    @Override
    public double pay() {
        return super.pay();
    }

    public static Comparator<Tutor> nameComparator = new Comparator<Tutor>(){

        @Override
        public int compare(Tutor o1, Tutor o2){return o1.getName().compareTo(o2.getName()); }

    };

    public static Comparator<Tutor> addressComparator = new Comparator<Tutor>(){

        @Override
        public int compare(Tutor o1, Tutor o2){return o1.getAddress().compareTo(o2.getAddress()); }

    };

    public static Comparator<Tutor> phoneComparator = new Comparator<Tutor>(){

        @Override
        public int compare(Tutor o1, Tutor o2){return o1.getPhone().compareTo(o2.getPhone()); }

    };

    @Override
    public int compareTo(Tutor o) {
        return this.getId() - o.getId();
    }
}
