package com.project.xmen.OOP.InterfacesHomework;

import com.project.xmen.OOP.Employee;
import com.project.xmen.OOP.StaffMember;

import java.util.Comparator;

public class Seller extends Employee implements FractionalWork, NeedSecondaryEd, Comparable<Seller> {

    private String salesLicense;

    public Seller(String name, String address, String phone, int id, String socialSecurityNumber, double payRate, String salesLicense) {
        super(name, address, phone, id, socialSecurityNumber, payRate);
        this.salesLicense = salesLicense;
    }

    @Override
    public void takeBreake(int begin, int end) {
        System.out.println("I have a break from " + begin + " to " + end);
    }

    @Override
    public void showСertificate(String nameOfSchool) {
        System.out.println("I graduated from the " + nameOfSchool + " school!");
    }

    @Override
    public double pay() {
        return 0;
    }

    public static Comparator<Seller> nameComparator = new Comparator<Seller>(){

        @Override
        public int compare(Seller o1, Seller o2){return o1.getName().compareTo(o2.getName()); }

    };

    public static Comparator<Seller> addressComparator = new Comparator<Seller>(){

        @Override
        public int compare(Seller o1, Seller o2){return o1.getAddress().compareTo(o2.getAddress()); }

    };

    public static Comparator<Seller> phoneComparator = new Comparator<Seller>(){

        @Override
        public int compare(Seller o1, Seller o2){return o1.getPhone().compareTo(o2.getPhone()); }

    };

    public static Comparator<Seller> licenseComparator = new Comparator<Seller>(){

        @Override
        public int compare(Seller o1, Seller o2){return o1.salesLicense.compareTo(o2.salesLicense); }

    };

    @Override
    public int compareTo(Seller o) {
        return this.getId() - o.getId();
    }
}
