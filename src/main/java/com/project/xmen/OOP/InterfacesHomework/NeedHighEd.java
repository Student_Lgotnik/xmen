package com.project.xmen.OOP.InterfacesHomework;

public interface NeedHighEd {

    public void showDiploma(String nameOfUniversity);

}
