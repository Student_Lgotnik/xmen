package com.project.xmen.OOP.InterfacesHomework;

public interface FractionalWork {

    public void takeBreake(int begin, int end);

}
