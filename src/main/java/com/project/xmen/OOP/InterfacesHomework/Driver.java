package com.project.xmen.OOP.InterfacesHomework;

import com.project.xmen.OOP.Employee;
import com.project.xmen.OOP.StaffMember;

import java.util.Comparator;

public class Driver extends Employee implements ShiftWork, NeedSecondaryEd, Comparable<Driver> {

    private String driverLicense;

    public Driver(String name, String address, String phone, int id, String socialSecurityNumber, double payRate, String driverLicense) {
        super(name, address, phone, id, socialSecurityNumber, payRate);
        this.driverLicense = driverLicense;
    }

    @Override
    public void showСertificate(String nameOfSchool) {
        System.out.println("I graduated from the " + nameOfSchool + " school!");
    }

    @Override
    public double pay() { return this.getPayRate(); }

    @Override
    public void takeShift(int numberOfShift) {
        System.out.println("My shift was " + numberOfShift);
    }

    public static Comparator<Driver> nameComparator = new Comparator<Driver>(){

        @Override
        public int compare(Driver o1, Driver o2){return o1.getName().compareTo(o2.getName()); }

    };

    public static Comparator<Driver> addressComparator = new Comparator<Driver>(){

        @Override
        public int compare(Driver o1, Driver o2){return o1.getAddress().compareTo(o2.getAddress()); }

    };

    public static Comparator<Driver> phoneComparator = new Comparator<Driver>(){

        @Override
        public int compare(Driver o1, Driver o2){return o1.getPhone().compareTo(o2.getPhone()); }

    };

    public static Comparator<Driver> licenseComparator = new Comparator<Driver>(){

        @Override
        public int compare(Driver o1, Driver o2){return o1.driverLicense.compareTo(o2.driverLicense); }

    };

    @Override
    public int compareTo(Driver o) {
        return this.getId() - o.getId();
    }


}
