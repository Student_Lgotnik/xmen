package com.project.xmen.OOP.InterfacesHomework;

import com.project.xmen.OOP.Employee;
import com.project.xmen.OOP.StaffMember;

import java.util.Comparator;

public class Pilot extends Employee implements AvailableSocialPack, FractionalWork, NeedHighEd, Comparable<Pilot> {

    private int militaryEx;

    public Pilot(String name, String address, String phone, int id, String socialSecurityNumber, double payRate, int militaryID) {
        super(name, address, phone, id, socialSecurityNumber, payRate);
        this.militaryEx = militaryID;
    }

    @Override
    public void getSocialPack() {
        System.out.println("I get my social pack!");
    }

    @Override
    public void takeBreake(int begin, int end) {
        System.out.println("I have a break from " + begin + " to " + end);
    }

    @Override
    public void showDiploma(String nameOfUniversity) {
        System.out.println("I graduated from the " + nameOfUniversity + " university!");
    }

    @Override
    public double pay() {
        return this.getPayRate() + militaryEx * 1000;
    }

    public static Comparator<Pilot> nameComparator = new Comparator<Pilot>(){

        @Override
        public int compare(Pilot o1, Pilot o2){return o1.getName().compareTo(o2.getName()); }

    };

    public static Comparator<Pilot> addressComparator = new Comparator<Pilot>(){

        @Override
        public int compare(Pilot o1, Pilot o2){return o1.getAddress().compareTo(o2.getAddress()); }

    };

    public static Comparator<Pilot> phoneComparator = new Comparator<Pilot>(){

        @Override
        public int compare(Pilot o1, Pilot o2){return o1.getPhone().compareTo(o2.getPhone()); }

    };

    public static Comparator<Pilot> militaryComparator = new Comparator<Pilot>(){

        @Override
        public int compare(Pilot o1, Pilot o2){return o1.militaryEx - o2.militaryEx; }

    };

    @Override
    public int compareTo(Pilot o) {
        return this.getId() - o.getId();
    }
}
