package com.project.xmen.OOP.InterfacesHomework;

import com.project.xmen.OOP.Employee;
import com.project.xmen.OOP.StaffMember;

import java.util.Comparator;

public class Doctor extends Employee implements AvailableSocialPack, ShiftWork, NeedHighEd, Comparable<Doctor> {

    private int doctorDegree;

    public Doctor(String name, String address, String phone, int id, String socialSecurityNumber, double payRate, int doctorDegree) {
        super(name, address, phone, id, socialSecurityNumber, payRate);
        this.doctorDegree = doctorDegree;
    }

    @Override
    public void getSocialPack() {
        System.out.println("I get my social pack!");
    }

    @Override
    public void showDiploma(String nameOfUniversity) {
        System.out.println("I graduated from the " + nameOfUniversity + " university!");
    }

    @Override
    public void takeShift(int numberOfShift) {
        System.out.println("My shift was " + numberOfShift);
    }



    public static Comparator<Doctor> nameComparator = new Comparator<Doctor>(){

        @Override
        public int compare(Doctor o1, Doctor o2){return o1.getName().compareTo(o2.getName()); }

    };

    public static Comparator<Doctor> addressComparator = new Comparator<Doctor>(){

        @Override
        public int compare(Doctor o1, Doctor o2){return o1.getAddress().compareTo(o2.getAddress()); }

    };

    public static Comparator<Doctor> phoneComparator = new Comparator<Doctor>(){

        @Override
        public int compare(Doctor o1, Doctor o2){return o1.getPhone().compareTo(o2.getPhone()); }

    };

    public static Comparator<Doctor> degreeComparator = new Comparator<Doctor>(){

        @Override
        public int compare(Doctor o1, Doctor o2){return o1.doctorDegree - o2.doctorDegree; }

    };

    @Override
    public int compareTo(Doctor o) { return super.getId() - o.getId(); }


    @Override
    public double pay() {
        return this.getPayRate() * ((doctorDegree + 1) / doctorDegree);
    }
}
