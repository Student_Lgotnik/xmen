package com.project.xmen.reports;

import org.omg.CORBA.INTERNAL;

import java.math.BigDecimal;

public class Item {

    private String description;
    private Integer quantity;
    private BigDecimal unitprice;

    public Item(String description, Integer quantity, BigDecimal unitprice) {
        this.description = description;
        this.quantity = quantity;
        this.unitprice = unitprice;
    }

    public Item() {
    }

    public void setDescription(String description) {
        this.description = description;
    }



    public String getDescription() {

        return description;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public void setUnitprice(BigDecimal unitprice) {
        this.unitprice = unitprice;
    }

    public Integer getQuantity() {

        return quantity;
    }

    public BigDecimal getUnitprice() {
        return unitprice;
    }
}
