package com.project.xmen.reports;

import java.math.BigDecimal;
import java.util.List;

public class Invoice {

    private Integer id;
    private BigDecimal shipping;
    private Double tax;
    private Customer billTo;
    private Customer ShipTo;
    private List<Item> items;

    public void setId(Integer id) {
        this.id = id;
    }

    public Customer getShipTo() { return ShipTo; }

    public void setShipping(BigDecimal shipping) {
        this.shipping = shipping;
    }

    public void setTax(Double tax) {
        this.tax = tax;
    }

    public void setBillTo(Customer billTo) {
        this.billTo = billTo;
    }

    public void setItems(List<Item> items) {
        this.items = items;
    }

    public Integer getId() { return id; }

    public BigDecimal getShipping() { return shipping; }

    public Double getTax() {
        return tax;
    }

    public Customer getBillTo() {
        return billTo;
    }

    public void setShipTo(Customer shipTo) { ShipTo = shipTo; }

    public List<Item> getItems() {
        return items;
    }
}
