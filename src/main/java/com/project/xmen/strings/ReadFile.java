package com.project.xmen.strings;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

public class ReadFile {

    public static void readCsv(String path){

        try (BufferedReader br = new BufferedReader(new FileReader(path))){

            String line;

            List<User> userList = new ArrayList<>();

            while ((line = br.readLine()) != null){

                //System.out.println(line);

                StringTokenizer stringTokenizer = new StringTokenizer(line, "|");

                while (stringTokenizer.hasMoreElements()){

                    Integer id = Integer.parseInt(stringTokenizer.nextElement().toString());
                    Double price = Double.parseDouble(stringTokenizer.nextElement().toString());
                    String username = stringTokenizer.nextElement().toString();


                    User user = new User(id, price, username);
                    userList.add(user);
//                    StringBuilder sb = new StringBuilder();
//                    sb.append("\nId : " + id);
//                    sb.append("\nPrice : " + price);
//                    sb.append("\nUsername : " + username);
//                    sb.append("\n*********************\n");
//
//                    System.out.println(sb.toString());

                }
            }
            for(User u : userList){
                System.out.println(u.toString());
            }
            System.out.println("Done");
        }
        catch (IOException e){
            e.printStackTrace();
        }
    }
}
