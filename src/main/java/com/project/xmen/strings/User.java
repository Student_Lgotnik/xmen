package com.project.xmen.strings;

import java.util.ArrayList;
import java.util.List;

public class User {

    public User() {
    }

    public User(int id, double price, String username) {
        this.id = id;
        this.price = price;
        this.username = username;
    }

    private int id;
    private double price;
    private String username;

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", price=" + price +
                ", username='" + username + '\'' +
                '}';
    }
}
