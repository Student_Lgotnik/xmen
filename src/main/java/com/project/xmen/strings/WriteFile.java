package com.project.xmen.strings;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class WriteFile {

    public static void writeTxt(String path, List<Employee> employees){
        try (BufferedWriter bw = new BufferedWriter(new FileWriter(path))){
            bw.write("");   //Clean file
            for (Employee e : employees){
                bw.append(e.toString() + "\n");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
