package com.project.xmen.enums;

public class StatusWithoutEnums {

    public static final int STATUS_OPEN = 0;
    public static final int STATUS_STARTED = 1;
    public static final int STATUS_INPROGRESS = 2;
    public static final int STATUS_ONHOLD = 3;
    public static final int STATUS_COMLETED = 4;
    public static final int STATUS_CLOSED = 5;

}
