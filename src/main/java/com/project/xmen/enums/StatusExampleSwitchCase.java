package com.project.xmen.enums;

public class StatusExampleSwitchCase {

    public enum Status {
        STATUS_OPEN(0, "Open"),
        STATUS_STARTED(1, "Started"),
        STATUS_INPROGRESS(2, "Inprogress"),
        STATUS_ONHOLD(3, "Onhold"),
        STATUS_COMPLETED(4, "Completed"),
        STATUS_CLOSED(5, "Closed");

        private final int status;
        private final String description;

        Status(int status, String desc) {
            this.status = status;
            this.description = desc;
        }

        public int status() {
            return this.status;
        }

        public String description() {
            return this.description;
        }
    }


    private static void checkStatus(Status status) {
        switch (status){
            case STATUS_OPEN:
                System.out.println("This is open status");
                break;
            case STATUS_STARTED:
                System.out.println("This is started status");
                break;
            case STATUS_INPROGRESS:
                System.out.println("This is inprogress status");
                break;
            case STATUS_ONHOLD:
                System.out.println("This is in onhold status");
                break;
            case STATUS_COMPLETED:
                System.out.println("This is in completed status");
                break;
            case STATUS_CLOSED:
                System.out.println("This is in closed status");
        }
    }

    public static void main(String[] args) {
        checkStatus(Status.STATUS_OPEN);
    }

}
