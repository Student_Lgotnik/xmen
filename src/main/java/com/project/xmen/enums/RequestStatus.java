package com.project.xmen.enums;

public class RequestStatus {

    private final int status;

    public RequestStatus(int status) {
        this.status = status;
    }

    public static final RequestStatus STATUS_OPEN = new RequestStatus(0);
    public static final RequestStatus STATUS_STARTED = new RequestStatus(1);
    public static final RequestStatus STATUS_INPROGRESS = new RequestStatus(2);
    public static final RequestStatus STATUS_ONHOLD = new RequestStatus(3);
    public static final RequestStatus STATUS_COMPLETED = new RequestStatus(4);
    public static final RequestStatus STATUS_CLOSED = new RequestStatus(5);

    public void test() {

        RequestStatus requestStatus = RequestStatus.STATUS_CLOSED;
        int status = requestStatus.status;
        System.out.println(status);

    }



}
