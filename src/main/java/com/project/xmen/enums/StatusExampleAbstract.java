package com.project.xmen.enums;

public class StatusExampleAbstract {

    public enum Status {

        STSTUS_OPEN{
            public String description() {
                return "open";
            }
        },
        STATUS_STARTED{
            public String description() {
                return "started";
            }
        },
        STATUS_INPROGRESS{
            public String description() {
                return "inprogress";
            }
        },
        STATUS_ONHOLD{
            public String description() {
                return "onhold";
            }
        },STATUS_COMPLETED{
            public String description() {
                return "completed";
            }
        },
        STATUS_CLOSED{
            public String description() {
                return "closed";
            }
        };


        Status() {
        }

        public abstract String description();

        public static void data(String value) {
            for (Status stat : Status.values()){
                if (stat.name().equalsIgnoreCase(value)){
                    System.out.println(stat.description());
                }
            }
        }
    }

    public static void main(String[] args) {
        Status.data("Status_onHold");
    }
}
