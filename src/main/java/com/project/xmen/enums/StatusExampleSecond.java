package com.project.xmen.enums;

public class StatusExampleSecond {

    public enum Status {
        STATUS_OPEN(0, "Open"),
        STATUS_STARTED(1, "Started"),
        STATUS_INPROGRESS(2, "Inprogress"),
        STATUS_ONHOLD(3, "Onhold"),
        STATUS_COOMPLETED(4, "Completed"),
        STATUS_CLOSED(5, "Closed");

        private final int status;
        private final String description;

        Status(int status, String desc) {
            this.status = status;
            this.description = desc;
        }

        public int status() {
            return this.status;
        }

        public String description() {
            return this.description;
        }
    }

    public static void main(String[] args) {
        for (Status stat : Status.values()){
            System.out.println(stat + "value is: " + stat.status() + " decription: " + stat.description());
        }
    }

}
