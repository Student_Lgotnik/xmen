package com.project.xmen.enums;

import org.junit.platform.engine.discovery.UriSelector;

public class SolarSystem {

    public enum Planet{
        MERCURY(0, "Mercury"){
            @Override
            public int getPosition() {
                return Planet.MERCURY.position;
            }
            @Override
            public String getName() {
                return Planet.MERCURY.name;
            }
        },
        VENUS(1, "Venus"){
            @Override
            public int getPosition() {
                return Planet.VENUS.position;
            }

            @Override
            public String getName() {
                return Planet.VENUS.name;
            }
        },
        EARTH(2, "Earth"){
            @Override
            public int getPosition() {
                return Planet.EARTH.position;
            }

            @Override
            public String getName() {
                return Planet.EARTH.name;
            }
        },
        MARS(3, "Mars"){
            @Override
            public String getName() {
                return Planet.MARS.name;
            }

            @Override
            public int getPosition() {
                return Planet.MARS.position;
            }
        },
        JUPITER(4, "Jupiter"){
            @Override
            public int getPosition() {
                return Planet.JUPITER.position;
            }

            @Override
            public String getName() {
                return Planet.JUPITER.name;
            }
        },
        SATURN(5, "SATURN"){
            @Override
            public int getPosition() {
                return Planet.SATURN.position;
            }

            @Override
            public String getName() {
                return Planet.SATURN.name;
            }
        },
        URANUS(6, "Uranus"){
            @Override
            public int getPosition() {
                return Planet.URANUS.position;
            }

            @Override
            public String getName() {
                return Planet.URANUS.name;
            }
        },
        NEPTUNE(7, "Neptune"){
            @Override
            public int getPosition() {
                return Planet.NEPTUNE.position;
            }

            @Override
            public String getName() {
                return Planet.NEPTUNE.name;
            }
        };


        private int position;
        private String name;

        Planet(int position, String name) {
            this.position = position;
            this.name = name;
        }

        public abstract int getPosition();
        public abstract String getName();
    }

}
