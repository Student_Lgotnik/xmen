package com.project.xmen.operators;

public class TernaryOperator {

    private final Integer FIRST = 5;
    private final Integer SECOND = 10;


    /**
     * Get Minimal Value
     * @param i
     * @param j
     * @return
     */
    public static int getMinValue(int i, int j){
        return (i < j) ? i : j;
    }


    /**
     * Get Absolute Value
     * @param i
     * @return
     */
    public static int getAbsoluteValue(int i){
        return i < 0 ? -i : i;
    }

    /**
     * Invert Boolean
     * @param b
     * @return
     */
    public static boolean invertBoolean(boolean b){
        return b ? false : true;
    }

    /**
     * Check if String contains "A"
     * @param str
     */
    public static void containsA(String str){
        String data = str.contains("A") ? "Str contains 'A'" : "Str does'n contains 'A'";
        System.out.println(data);
    }

    /**
     * Ternary method example
     * @param i
     * @param ternaryOperator
     */
    public static void ternaryMethod(Integer i, TernaryOperator ternaryOperator){
        System.out.println((i.equals(ternaryOperator.getFirst()))
                ? "i=5" : ((i.equals(ternaryOperator.getSecond())) ? "i=10" : "i is not equal to 5 or 10"));
    }

    /**
     *
     * @return
     */
    public Integer getFirst() {
        return FIRST;
    }

    /**
     *
     * @return
     */
    public Integer getSecond() {
        return SECOND;
    }
}
