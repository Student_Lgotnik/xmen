package com.project.xmen.multithreading;

public class ThreadDemo extends Thread {

    private Thread t;
    private String threadNAme;

    public ThreadDemo(String threadNAme) {
        this.threadNAme = threadNAme;
        System.out.println("Creating " + threadNAme);
    }

    public void run() {
        System.out.println("Running " + threadNAme);
        try {
            for (int i = 4; i > 0; i--) {
                System.out.println("Thread " + threadNAme + "," + i);
                //Let the thread sleep for a while
                Thread.sleep(5000);
            }
        } catch (InterruptedException e) {
            System.out.println("Thread " + threadNAme + "interrupted");
        }

        System.out.println("Thread " + threadNAme + " exiting");
    }

    public void start() {
        System.out.println("Starting " + threadNAme);
        if (t == null){
            t = new Thread(this, threadNAme);
            t.start();
        }
    }
}
