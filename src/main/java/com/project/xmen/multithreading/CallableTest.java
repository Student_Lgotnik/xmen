package com.project.xmen.multithreading;

import sun.nio.ch.ThreadPool;

import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.*;

public class CallableTest {
    public static void main(String[] args) {
        Callable<Integer> task = () -> {
            try {
                TimeUnit.SECONDS.sleep(1);
                return 123;
            }
            catch (InterruptedException e){
                throw new IllegalStateException("Task interrupted", e);
            }
        };

        try {
            Integer x = task.call();
            System.out.println(x);
        } catch (Exception e) {
            e.printStackTrace();
        }

        ExecutorService executor = Executors.newSingleThreadExecutor();
        Future<Integer> future = executor.submit(task);

        System.out.println("future done? " + future.isDone());

        Integer result = null;
        try {
            result = future.get();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        } finally {
            executor.shutdownNow();
        }

        System.out.println("Future done? " + future.isDone());
        System.out.println("result: " + result);

        invokeAll();
        ExecutorService executor2 =  Executors.newWorkStealingPool();

        List<Callable<String>> callables =Arrays.asList(
                callable("task1", 2),
                callable("task2", 1),
                callable("task3", 3));
//        String result2 = null;
        try {
            executor2.invokeAll(callables)
                    .stream()
                    .map(futures -> {
                        try {
                            return futures.get();
                        } catch (Exception ex) {
                            throw new IllegalStateException(ex);
                        }
                    })
                    .forEach(x -> {
                        try {
                            TimeUnit.SECONDS.sleep(2);
                            System.out.println(x);

                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    });
        } catch (InterruptedException e) {
            e.printStackTrace();
        } finally {
            executor2.shutdownNow();
        }

    }

    public static void  invokeAll() {
        ExecutorService executor = Executors.newSingleThreadExecutor();

        List<Callable<String>> callables = Arrays.asList(
                () -> "task1",
                () -> "task2",
                () -> "task3");
        try {
            executor.invokeAll(callables)
                    .stream()
                    .map(future -> {
                        try {
                            return future.get();
                        } catch (Exception ex) {
                            throw new IllegalStateException(ex);
                        }
                    })
                    .forEach(x -> {
                        try {
                            TimeUnit.SECONDS.sleep(2);
                            System.out.println(x);

                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    });
        } catch (InterruptedException e) {
            e.printStackTrace();
        } finally {
            executor.shutdownNow();
        }
    }

    static Callable<String> callable(String result, long sleepSeconds){
        return () -> {
            TimeUnit.SECONDS.sleep(sleepSeconds);
            return result;
        };
    }
}
