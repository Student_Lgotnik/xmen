package com.project.xmen.pattern.factory;

public abstract class Animal {
    public abstract String makeSound();
}
