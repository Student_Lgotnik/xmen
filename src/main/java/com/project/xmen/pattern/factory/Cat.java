package com.project.xmen.pattern.factory;

public class Cat extends Animal{
    @Override
    public String makeSound(){
        return "Meow";
    }
}
