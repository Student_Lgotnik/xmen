package com.project.xmen.pattern.abstractfactory;

import com.project.xmen.pattern.factory.Animal;
import com.project.xmen.pattern.factory.Snake;
import com.project.xmen.pattern.factory.Tyrannosaurus;

public class ReptileFactory extends SpeciesFactory {

    @Override
    public Animal getAnimal(String type) {
        if("snake".equals(type)){
            return new Snake();
        } else{
            return new Tyrannosaurus();
        }
    }
}

