package com.project.xmen.pattern.abstractfactory;

import com.project.xmen.pattern.factory.Animal;
import com.project.xmen.pattern.factory.Cat;
import com.project.xmen.pattern.factory.Dog;

public class MammalFactory extends SpeciesFactory{

    @Override
    public Animal getAnimal(String type) {
        if ("dog".equals(type)){
            return new Dog();
        } else {
            return new Cat();
        }
    }
}
