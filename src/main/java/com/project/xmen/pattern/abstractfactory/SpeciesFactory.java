package com.project.xmen.pattern.abstractfactory;

import com.project.xmen.pattern.factory.Animal;

public abstract class SpeciesFactory {
    public abstract Animal getAnimal(String type);
}