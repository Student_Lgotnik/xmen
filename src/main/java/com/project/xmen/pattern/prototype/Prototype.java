package com.project.xmen.pattern.prototype;

public interface Prototype {
    Prototype doClone();
}