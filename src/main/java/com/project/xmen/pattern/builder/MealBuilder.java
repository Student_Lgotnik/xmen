package com.project.xmen.pattern.builder;

public interface MealBuilder {

    void buildDrink();

    void buildMainCourse();

    void buildSide();

    Meal getMeal();
}
