package com.project.xmen.poi;


import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import javax.swing.*;
import java.awt.*;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Calendar;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

public class NewWxcel {

    public static void main(String[] args) {
        XSSFWorkbook workbook = new XSSFWorkbook();

        XSSFSheet spreadsheet = workbook.createSheet("Employee Info");

        Map<String, Object[]> empinfo = new TreeMap<>();

        empinfo.put("1", new Object[] {"EMP ID", "ENP NAME", "DESTINATION"});
        empinfo.put("2", new Object[] {"tp01", "Vovan", "TEchnical Manager"});
        empinfo.put("3", new Object[] {"tp02", "Ivan", "Proof Reader"});
        empinfo.put("4", new Object[] {"tp03", "Jack", "Technical Writer"});
        empinfo.put("5", new Object[] {"tp04", "Leo", "Technical Writer"});
        empinfo.put("6", new Object[] {"tp05", "Vovan", "Technical Writer"});

        Set<String> keyid = empinfo.keySet();

        XSSFRow row;

        int rowid = 0;

        for (String key : keyid) {
            row = spreadsheet.createRow(rowid++);
            Object[] object = empinfo.get(key);
            int cellid = 0;
            for (Object obj : object){
                Cell cell = row.createCell(cellid++);
                cell.setCellValue((String)obj);
            }
        }

        try (FileOutputStream out = new FileOutputStream(new File("Writesheet.xlsx"))){
            workbook.write(out);
            out.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println("Writesheet.xlsx written successfully");

    }

}
