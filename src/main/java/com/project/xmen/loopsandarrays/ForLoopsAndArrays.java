package com.project.xmen.loopsandarrays;

import java.lang.reflect.Array;
import java.util.Arrays;

public class ForLoopsAndArrays {

    public static String equalsOfTwoArrays(int[] myarray1, int[] myarray2){
        if(myarray1 == null && myarray2 == null) {
            return "Arrays are null!";
        } else if(myarray1 == null || myarray2 == null){
            return (myarray1 == null) ? "First array is null." : "Second array is null." + " They are not equals!";
        } else if(myarray1.length == myarray2.length){
            for (int i = 0; i < myarray1.length; i++) {
                if (myarray1[i] != myarray2[i]) {
                    return "Arrays are not equals!";
                }
            }
        }
        return "Equals!";
    }
    public static void uniqueArray(int[] myArray){
        int arraySize = myArray.length;

        System.out.println("Original Array : ");

        for (int i = 0; i < arraySize ; i++) {
            System.out.print(myArray[i] + "\t");
        }
        //Comparing each element with all other elements
        for (int i = 0; i < arraySize; i++) {
            for (int j = i + 1; j < arraySize; j++) {
                //If any two elements are found equal
                if (myArray[i] == myArray[j]){
                    //Replace dublicate element with last unique element
                    myArray[j] = myArray[arraySize - 1];

                    arraySize--;

                    j--;
                }
            }
        }
        //Copying only unique elements of my_array into array1
        int[] array1 = Arrays.copyOf(myArray, arraySize);

        System.out.println();

        System.out.println("Array with unique values : ");
        for (int i = 0; i < array1.length; i++) {
            System.out.print(array1[i] + "\t");
        }
    }
}
