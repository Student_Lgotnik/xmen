package com.project.xmen.loopsandarrays;

import java.util.Scanner;

public class WhileLoops {
    public static void main(String[] args) {
        while (true) {
            System.out.print("Hi! It is a loop program.\nEnter 's' to start or 'e' to exit: ");
            Scanner scanner = new Scanner(System.in);
            char c = scanner.next().charAt(0);
            if (c == 's') {
                boolean result = selectChoise(c);
                if (result){
                    System.out.println("Exiting...!");
                    break;
                }
            } else if(c == 'e'){
                System.out.println("Exiting...!");
                break;
            } else {
                System.out.println("Incorrect! Try again.");
            }
        }
    }
    public static boolean selectChoise(char c){
        if (c == 'a'){
            return true;
        } else if (c == 'b'){
            System.out.println("Returning to start of the program...!");
            return false;
        }
        char oper = inputAndOutput();
        if (selectChoise(oper)){
            return true;
        }
        return false;
    }

    public static char inputAndOutput(){
        Scanner scanner = new Scanner(System.in);
        System.out.println("Program menu.\nChoose an operation:");
        System.out.println("a - exit");
        System.out.println("b - breake");
        System.out.println("c - continue");
        char res = scanner.next().charAt(0);
        if(res != 'a' && res != 'b' && res != 'c'){
            System.out.println("Incorrect! Enter other symbol");
            return inputAndOutput();
        }
        return res;
    }
}
