package com.project.xmen.loopsandarrays;

public class Matrix {
    public static Double[][] multiplicar(Double[][] first, Double[][] second) {

        int fitrstRows = first.length;
        int firstColumns = first[0].length;
        int secondRows = second.length;
        int secondColumns = second[0].length;

        if (firstColumns != secondRows){
            throw new IllegalArgumentException("First Rows: " + fitrstRows + " did not math Second Colums " + secondColumns + ".");

        }

        Double[][] result = new Double[fitrstRows][secondColumns];
        for (int i = 0; i < fitrstRows; i++) {
            for (int j = 0; j < secondColumns; j++) {
                result[i][j] = 0.00000;
            }
        }

        for (int i = 0; i < fitrstRows; i++) { //firsRow
            for (int j = 0; j < secondColumns; j++) { //secondColumn
                for (int k = 0; k < result.length; k++) { //resultColumn
                    result[i][j] += first[i][k] * second[k][j];
                }
            }
        }
        return result;
    }

}
