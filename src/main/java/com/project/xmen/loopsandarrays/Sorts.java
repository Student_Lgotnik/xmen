package com.project.xmen.loopsandarrays;

public class Sorts {

    public static Integer[] bubbleSort(Integer[] arr){
        for (int i = 0; i < arr.length - 1; i++) {
            for (int j = 0; j < arr.length - 1; j++) {
                if (arr[j] > arr[j+1]){
                    int temp = arr[j];
                    arr[j] = arr[j+1];
                    arr[j+1] = temp;
                }
            }
        }
        return arr;
    }

    public static Integer[] shakerSort(Integer[] arr){
        int temp;
        int left = 0;
        int right = arr.length - 1;
        while (left < right){
            for (int i = left; i < right; i++){
                if (arr[i] > arr[i+1]){
                    temp = arr[i];
                    arr[i] = arr[i+1];
                    arr[i+1] = temp;
                }
            }
            right--;
            for (int i = right; i > left; i--){
                if (arr[i] < arr[i-1]){
                    temp = arr[i];
                    arr[i] = arr[i-1];
                    arr[i-1] = temp;
                }
            }
            left++;
        }
        return arr;
    }
}
