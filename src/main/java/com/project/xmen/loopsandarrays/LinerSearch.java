package com.project.xmen.loopsandarrays;

public class LinerSearch {

    public static int sequentialSearch(int[] arr, Integer searched){
        for (int i = 0; i < arr.length; i++) {
            if (arr[i] == searched){
                return i;
            }
        }
        return -1;
    }

    public static int sequentialSearch(String[] arr, String searched){
        for (int i = 0; i < arr.length; i++) {
            if (arr[i].equals(searched)){
                return i;
            }
        }
        return -1;
    }
}
