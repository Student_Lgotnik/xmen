package com.project.xmen.functional;

import com.fasterxml.jackson.databind.cfg.DatabindVersion;


import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class TestRegularSorting {


    public static void main(String[] args) {

        List<Developer> listDevs = getDevelopers();

        System.out.println("Befpre sort");
        for (Developer developer : listDevs){
            System.out.println(developer);
        }

        //sort by age
        Collections.sort(listDevs, new Comparator<Developer>() {
            @Override
            public int compare(Developer o1, Developer o2) {
                return o1.getAge() - o2.getAge();
            }
        });

        System.out.println("After sort");
        for (Developer developer : listDevs){
            System.out.println(developer);
        }

    }

    private static List<Developer> getDevelopers() {

        List<Developer> result = new ArrayList<>();

        result.add(new Developer("alex", 10000, 33));
        result.add(new Developer("alvin", 80000, 20));
        result.add(new Developer("jason", 100000, 10));
        result.add(new Developer("iris", 1700000, 55));

        return result;

    }

}
