package com.project.xmen.functional;

@FunctionalInterface
public interface ComplexFunctionalInteface extends SimpleFunctioanlInterface{

    int x = 5;
    static void doSomeWork() {
        System.out.println("Do some work in interface impl...");
    }

    @Override
    default void doSomeOtherWork() {
        System.out.println("Doin some other work in inteface impl...");
    }

    @Override
    void doWork();

    @Override
    String toString();

    @Override
    boolean equals(Object o);
}
