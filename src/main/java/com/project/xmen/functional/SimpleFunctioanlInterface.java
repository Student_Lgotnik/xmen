package com.project.xmen.functional;

@FunctionalInterface
public interface SimpleFunctioanlInterface {

    int x = 5;

    static void  doSomeWork() {
        System.out.println("Doing some work in interface impl...");
    }

    default void doSomeOtherWork() {
        System.out.println("Doing some other work in interface impl...");
    }

    void doWork();
    String toString();
    boolean equals(Object o);

}
