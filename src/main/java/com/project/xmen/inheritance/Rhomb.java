package com.project.xmen.inheritance;

public class Rhomb extends Square{

    private double side;

    public Rhomb(double height, double side) {
        super(height);
        this.side = side;
    }

    public Rhomb(double side){
        this.side = side;
    }

    public Rhomb(){
        super();
        side = 2.0;
    }

    public double getSide() {
        return side;
    }


    public double getVolume() {
        return getHeight()*side;
    }


    @Override
    public String toString() {
        return "Rhomb{" +
                "side=" + side +
                '}';
    }
}
