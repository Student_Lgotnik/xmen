package com.project.xmen.inheritance;

public class Triangle extends Figure{
    private double side;
    private double hight;


    public Triangle() {
    }

    public Triangle(double side, double hight) {
        this.side = side;
        this.hight = hight;
    }

    public double getSide() {
        return side;
    }

    public void setSide(double side) {
        this.side = side;
    }


    public double getHight() {
        return hight;
    }

    public void setHight(double hight) {
        this.hight = hight;
    }

    @Override
    double getArea() {
        return colculateArea();
    }

    @Override
    double getPerimetr() {
        return 0;
    }

    private double colculateArea(){
        return 0.5*this.side*this.hight;
    }

    @Override
    public String toString() {
        return "Triangle{" +
                "side=" + side +
                ", hight=" + hight +
                '}';
    }
}
