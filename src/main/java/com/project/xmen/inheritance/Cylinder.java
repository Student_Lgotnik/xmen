package com.project.xmen.inheritance;

public class Cylinder extends Circle {
    private double height;
    //Constructor with default radius and height
    public Cylinder(){
        super();
        height = 1.0;
    }

    public Cylinder(double height){
        this.height=height;
    }

    public Cylinder(double radius, double height) {
        super(radius);
        this.height = height;
    }

    public double getHeight() {
        return height;
    }

    public double getValume(){
        return getArea()*height;
    }

    public double getPerometr(){return getPerimetr()*(height+getRadius());}

    @Override
    public String toString() {
        return "Cylinder: subclass of " + super.toString() +
                "height=" + height;
    }
}
