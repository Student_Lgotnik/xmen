package com.project.xmen.inheritance;

public abstract class Figure {
    abstract double getArea();

    abstract double getPerimetr();
}
