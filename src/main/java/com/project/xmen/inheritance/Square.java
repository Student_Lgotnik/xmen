package com.project.xmen.inheritance;

public class Square extends Figure{
    private double height;

    public Square(double height) {
        this.height = height;
    }

    public Square() {
    }

    public double getHeight() {
        return height;
    }

    public void setHeight(double height) {
        this.height = height;
    }

    @Override
    double getArea() {
        return calculateArea();
    }

    public double calculateArea(){
        return this.height * this.height;
    }

    public double colculatePerimetr(){
        return this.height*4;
    }

    @Override
    double getPerimetr() {
        return colculatePerimetr();
    }

    @Override
    public String toString() {
        return "Square{" +
                "height=" + height +
                '}';
    }
}
