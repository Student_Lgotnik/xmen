package com.project.xmen.inheritance;

public class Circle extends Figure{
    private double radius = 1.0;

    public Circle(double radius) {
        this.radius = radius;
    }

    public Circle() {
    }

    public double getRadius() {
        return radius;
    }

    public void setRadius(double radius) {
        this.radius = radius;
    }

    @Override
    public double getArea(){
        return calculateArea();
    }

    private double calculateArea(){
        return this.radius*this.radius*Math.PI;
    }

    @Override
    public String toString() {
        return "Circle{" +
                "radius=" + radius +
                '}';
    }

    private double colculatePerimetr(){
        return 2*Math.PI*radius;
    }

    @Override
    double getPerimetr() {
        return 0;
    }
}
